VX_DETAIL_ENUM_FORMATTER_DECLARE(vx::Device::QueueCreateInfo::FlagBits);

inline uint32_t
vx::Device::QueueCreateInfo::queueCount() const
{
  return static_cast<uint32_t>(queuePriorities.size());
}
