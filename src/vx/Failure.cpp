#include "Failure.hpp"

namespace {
  [[noreturn]] void defaultFailureHandler()
  {
    std::exit(1);
  }

  vx::FailureHandlerPtr& currentFailureHandler()
  {
    static vx::FailureHandlerPtr retval = &defaultFailureHandler;
    return retval;
  }
}

namespace vx {

  [[noreturn]] void runFailureHandler()
  {
    (*currentFailureHandler())();

    // This should never be reached as "currentFailureHandler()" is assumed to
    // be a [[noreturn]] function.
    std::abort();
  }

  FailureHandlerGuard::~FailureHandlerGuard()
  {
    VX_ASSERT(oldFailureHandler != nullptr);
    currentFailureHandler() = oldFailureHandler;
  }

  FailureHandlerGuard::FailureHandlerGuard(
      FailureHandlerPtr const newFailureHandler)
    : oldFailureHandler(currentFailureHandler())
  {
    VX_REQUIRE(newFailureHandler != nullptr);

    VX_ASSERT(oldFailureHandler != nullptr);

    currentFailureHandler() = newFailureHandler;
  }
}

namespace vx::detail {
  struct DtorMonitor {
    using CallbackType        = void (*)();
    CallbackType dtorCallback = nullptr;

    ~DtorMonitor()
    {
      VX_REQUIRE(dtorCallback != nullptr);
      dtorCallback();
    }
  };

  void unitTestFunction(int const positive, void (*dtorCallback)())
  {
    DtorMonitor mon{.dtorCallback = dtorCallback};

    VX_REQUIRE(positive > 0);
  }
}
