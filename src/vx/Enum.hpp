#ifndef __INCLUDE_GUARD_VX_ENUM_HPP
#define __INCLUDE_GUARD_VX_ENUM_HPP

#include "Failure.hpp"

#include <fmt/format.h>

#include <map>
#include <string_view>

namespace vx {
  /// \rst
  /// Get all values of an enum.
  /// \endrst
  template<typename EnumT>
  std::vector<EnumT> const& enumValues();
}

#include "Enum.ipp"

#endif
