#ifndef __INCLUDE_GUARD_VX_BITSET_HPP
#define __INCLUDE_GUARD_VX_BITSET_HPP

#include "Enum.hpp"

#include <bit>

namespace vx {
  /// \rst
  /// Check whether the values of the enum *EnumT* form a set of flag bits
  /// or not. *EnumT* is considered to form a set of flag bits if every element
  /// of the vector returned by :cpp:func:`vx::enumValues()` is an
  /// integer that has exactly one bit set.
  ///
  /// Every type *EnumT* used with :cpp:struct:`vx::Bitset` must statisfy this
  /// property.
  ///
  /// This function should really be available at compile time, but C++20 does
  /// not allow this yet.
  /// \endrst
  template<typename EnumT>
  bool areFlagBits();

  /// \rst
  /// Represents a bitset where the individual bits are represented by the enum
  /// *EnumT*. The *EnumT* is expected to satisfy the property
  /// :cpp:func:`vx::areFlagBits`.
  /// \endrst
  template<typename EnumT>
  struct Bitset {
    /// The underlying type of the enum given by *EnumT*.
    using UnderlyingType = std::underlying_type_t<EnumT>;

    // This could theoretically be checked at compile time, but C++20 doesn't
    // allow this yet.
    //
    // static_assert(areFlagBits<EnumT>());

    static_assert(std::is_unsigned_v<UnderlyingType>);

    /// Data member.
    UnderlyingType data = 0;

    /// \rst
    /// Create a bitset from the given enum values.
    /// \endrst
    template<typename... EnumTs>
    constexpr static Bitset<EnumT> create(EnumT e, EnumTs... es) noexcept;

    /// \rst
    /// Test if bit *e* is set.
    /// \endrst
    constexpr bool test(EnumT e) const noexcept;

    /// Test if any bit is set.
    constexpr bool any() const noexcept;

    /// Test if no bit at all is set.
    constexpr bool none() const noexcept;

    /// Bitwise and of two bitsets.
    constexpr Bitset operator&(Bitset const& other) const noexcept;

    /// Bitwise or of two bitsets.
    constexpr Bitset operator|(Bitset const& other) const noexcept;

    /// \rst
    /// Bitwise or of a bitset and the bitset that has the single bit *e* set.
    /// Can be used as an alternative way to create bitsets as follows.
    ///
    /// .. code:: cpp
    ///
    ///   auto const b = vx::Bitset<MyEnum>{}
    ///      | MyEnum::VALUE_A
    ///      | MyEnum::VALUE_C;
    /// \endrst
    constexpr Bitset operator|(EnumT e) const noexcept;

    /// Defaulted comparison.
    constexpr auto operator<=>(Bitset const&) const noexcept = default;

    /// Returns the number of set bits in this bitset.
    constexpr int popcount() const noexcept;
  };

  /// \rst
  /// Same as :cpp:member:`vx::Bitset::create` but allows for type deduction of
  /// the template parameter.
  ///
  /// .. code:: cpp
  ///
  ///   vx::Bitset<MyEnum>::create(yEnum::VALUE_A, MyEnum::VALUE_C) ==
  ///   vx::createBitset(yEnum::VALUE_A, MyEnum::VALUE_C)
  /// \endrst
  template<typename EnumT, typename... EnumTs>
  constexpr Bitset<EnumT> createBitset(EnumT e, EnumTs... es) noexcept;
}

#include "Bitset.ipp"

#endif
