#ifndef __INCLUDE_GUARD_VX_DEVICE_HPP
#define __INCLUDE_GUARD_VX_DEVICE_HPP

#include "Bitset.hpp"
#include "PhysicalDevice.hpp"

namespace vx {

  /// \rst
  /// :vkspec:`chap5.html#VkDevice`
  /// \endrst
  struct Device {
    /// The Vulkan handle.
    VULKAN::VkDevice handle = VK_NULL_HANDLE;

    /// \rst
    /// :vkspec:`chap5.html#VkDeviceQueueCreateInfo`
    /// \endrst
    struct QueueCreateInfo {
      /// \rst
      /// :vkspec:`chap5.html#VkDeviceQueueCreateFlagBits`
      /// \endrst
      enum class FlagBits : uint32_t {
        PROTECTED = 0x00000001,
      };

      Bitset<FlagBits> flags;

      /// \rst
      /// Selects a queue family by its
      /// :cpp:member:`~vx::PhysicalDevice::QueueFamilyProperties::index`.
      /// \endrst
      uint32_t queueFamilyIndex = 0;

      /// \rst
      /// The size of this vector determines the number of queues to create. The
      /// elements of this vector then assign priorities to each created queue
      /// and must be in the interval [0.0, 1.0].
      /// \endrst
      std::vector<float> queuePriorities;

      /// \rst
      /// Length of :cpp:member:`~vx::Device::QueueCreateInfo::queuePriorities`.
      /// \endrst
      uint32_t queueCount() const;

      auto operator<=>(QueueCreateInfo const&) const noexcept = default;
    };

    /// \rst
    /// :vkspec:`chap5.html#VkDeviceCreateInfo`
    /// \endrst
    struct CreateInfo {
      std::vector<QueueCreateInfo> queueCreateInfos;
      std::vector<std::string> enabledExtensionNames;

      auto operator<=>(CreateInfo const&) const noexcept = default;
    };

    /// \rst
    /// The :cpp:struct:`~vx::Device::CreateInfo` used to
    /// :cpp:func:`~vx::Device::create()` this :cpp:struct:`~vx::Device`.
    /// \endrst
    CreateInfo createInfo;

    Device() = default;

    Device(Device&&);
    Device& operator=(Device&&);

    ~Device();

    /// \rst
    /// Whether the :cpp:member:`~vx::Device::handle` is :code:`VK_NULL_HANDLE`.
    /// \endrst
    bool isValid() const noexcept;

    /// \rst
    /// :vkspec:`chap5.html#vkDestroyDevice`
    /// \endrst
    void destroy() noexcept;

    /// \rst
    /// :vkspec:`chap5.html#vkCreateDevice`
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`~vx::PhysicalDevice::isValid()`.
    /// \endrst
    static Result<Device> create(PhysicalDevice const&, CreateInfo);

    /// \rst
    /// Variant of :cpp:func:`vx::Device::create()` that creates a device with
    /// only a single queue.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`~vx::PhysicalDevice::isValid()`.
    /// \endrst
    static Result<Device> createWithSingleQueue(
        PhysicalDevice const&,
        uint32_t queueFamilyIndex,
        std::vector<std::string> enabledExtensionNames = {});
  };
}

#include "Device.ipp"

#endif
