#include "Result.hpp"

using namespace vx;

namespace vx::detail {

  class StatusCodeCategory : public std::error_category {
   public:
    virtual char const* name() const noexcept override final
    {
      return "vx::StatusCode";
    }

    virtual std::string message(int const c) const noexcept override final
    {
      switch (static_cast<StatusCode>(c)) {
#define CASE(x)       \
  case StatusCode::x: \
    return #x;
        CASE(SUCCESS)
        CASE(NOT_READY)
        CASE(TIMEOUT)
        CASE(EVENT_SET)
        CASE(EVENT_RESET)
        CASE(INCOMPLETE)
        CASE(ERROR_OUT_OF_HOST_MEMORY)
        CASE(ERROR_OUT_OF_DEVICE_MEMORY)
        CASE(ERROR_INITIALIZATION_FAILED)
        CASE(ERROR_DEVICE_LOST)
        CASE(ERROR_MEMORY_MAP_FAILED)
        CASE(ERROR_LAYER_NOT_PRESENT)
        CASE(ERROR_EXTENSION_NOT_PRESENT)
        CASE(ERROR_FEATURE_NOT_PRESENT)
        CASE(ERROR_INCOMPATIBLE_DRIVER)
        CASE(ERROR_TOO_MANY_OBJECTS)
        CASE(ERROR_FORMAT_NOT_SUPPORTED)
        CASE(ERROR_FRAGMENTED_POOL)
        CASE(ERROR_UNKNOWN)
        CASE(ERROR_OUT_OF_POOL_MEMORY)
        CASE(ERROR_INVALID_EXTERNAL_HANDLE)
        CASE(ERROR_FRAGMENTATION)
        CASE(ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS)
        CASE(ERROR_SURFACE_LOST_KHR)
        CASE(ERROR_NATIVE_WINDOW_IN_USE_KHR)
        CASE(SUBOPTIMAL_KHR)
        CASE(ERROR_OUT_OF_DATE_KHR)
        CASE(ERROR_INCOMPATIBLE_DISPLAY_KHR)
        CASE(ERROR_VALIDATION_FAILED_EXT)
        CASE(ERROR_INVALID_SHADER_NV)
        CASE(ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT)
        CASE(ERROR_NOT_PERMITTED_EXT)
        CASE(ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT)
        CASE(THREAD_IDLE_KHR)
        CASE(THREAD_DONE_KHR)
        CASE(OPERATION_DEFERRED_KHR)
        CASE(OPERATION_NOT_DEFERRED_KHR)
        CASE(PIPELINE_COMPILE_REQUIRED_EXT)
        CASE(ERROR_NO_PHYSICAL_DEVICE)
        CASE(ERROR_NO_MATCHING_QUEUE_FAMILY)
#undef CASE
      }

      VX_UNREACHABLE();
    }

    virtual std::error_condition
    default_error_condition(int const c) const noexcept override final
    {
      if (c >= 0) {
        return std::error_condition{};
      }
      else {
        return std::error_condition(c, *this);
      }
    }

    static StatusCodeCategory const& instance() noexcept
    {
      static StatusCodeCategory c;
      return c;
    }
  };
}

namespace vx {
  ErrorMessage::ErrorMessage(std::error_code const errCode, std::string errMsg)
    : data(std::make_unique<std::tuple<std::error_code, std::string>>(
          errCode, std::move(errMsg)))
  {
  }

  std::strong_ordering
  ErrorMessage::operator<=>(ErrorMessage const& other) const noexcept
  {
    VX_ASSERT(data && other.data);
    return (*data <=> *other.data);
  }

  bool ErrorMessage::operator==(ErrorMessage const& other) const noexcept
  {
    VX_ASSERT(data && other.data);
    return (*data == *other.data);
  }

  Error::Error(std::error_code ec) noexcept : data(std::move(ec)) {}
  Error::Error(ErrorMessage em) noexcept : data(std::move(em)) {}

  std::error_code Error::code() const noexcept
  {
    return std::visit(detail::ErrorCodeVisitor{}, data);
  }
}

namespace vx {
  std::error_code make_error_code(StatusCode const c) noexcept
  {
    return {static_cast<int>(c), detail::StatusCodeCategory::instance()};
  }

  std::error_condition make_error_condition(StatusCode const c) noexcept
  {
    return {static_cast<int>(c), detail::StatusCodeCategory::instance()};
  }
}
