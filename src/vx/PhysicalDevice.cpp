#include "PhysicalDevice.hpp"

#include "Detail.hpp"

#include <algorithm>

using namespace vx;

VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(QueueFlagBits) = {
    {QueueFlagBits::GRAPHICS, "GRAPHICS"},
    {QueueFlagBits::COMPUTE, "COMPUTE"},
    {QueueFlagBits::TRANSFER, "TRANSFER"},
    {QueueFlagBits::SPARSE_BINDING, "SPARSE_BINDING"},
    {QueueFlagBits::PROTECTED, "PROTECTED"},
};
VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(PhysicalDevice::Type) = {
    {PhysicalDevice::Type::OTHER, "OTHER"},
    {PhysicalDevice::Type::INTEGRATED_GPU, "INTEGRATED_GPU"},
    {PhysicalDevice::Type::DISCRETE_GPU, "DISCRETE_GPU"},
    {PhysicalDevice::Type::VIRTUAL_GPU, "VIRTUAL_GPU"},
    {PhysicalDevice::Type::CPU, "CPU"},
};

bool
PhysicalDevice::isValid() const noexcept
{
  return (handle != VK_NULL_HANDLE);
}

PhysicalDevice::Properties
PhysicalDevice::getProperties() const
{
  VX_REQUIRE(isValid());

  VULKAN::VkPhysicalDeviceProperties temp = {};
  vkGetPhysicalDeviceProperties(handle, &temp);

  return detail::fromVulkan(temp);
}

Result<std::vector<PhysicalDevice::QueueFamilyProperties>>
PhysicalDevice::getQueueFamilyProperties(Surface const& surface) const
{
  VX_REQUIRE(isValid());

  uint32_t queueFamilyCount = 0;
  VULKAN::vkGetPhysicalDeviceQueueFamilyProperties(
      handle, &queueFamilyCount, NULL);
  VX_VULKAN_SPEC(queueFamilyCount >= 1);

  std::vector<VULKAN::VkQueueFamilyProperties> temp(queueFamilyCount);
  VULKAN::vkGetPhysicalDeviceQueueFamilyProperties(
      handle, &queueFamilyCount, temp.data());

  std::vector<PhysicalDevice::QueueFamilyProperties> retval(temp.size());
  for (size_t i = 0; i < temp.size(); ++i) {
    retval[i] = detail::fromVulkan(i, temp[i]);
  }

  if (surface.isValid()) {
    for (auto& qFamProps: retval) {
      VULKAN::VkBool32 result = VK_FALSE;
      auto const vkResult     = VULKAN::vkGetPhysicalDeviceSurfaceSupportKHR(
          handle, qFamProps.index, surface.handle, &result);
      if (vkResult != VK_SUCCESS) {
        return make_error_code(detail::fromVulkan(vkResult));
      }
      if (result == VK_TRUE) {
        qFamProps.queueFlags =
            qFamProps.queueFlags | QueueFlagBits::SURFACE_SUPPORT;
      }
    }
  }

  return {std::move(retval)};
}

Result<std::vector<ExtensionProperties>>
PhysicalDevice::enumerateExtensionProperties(std::string const& layerName) const
{
  VX_REQUIRE(isValid());

  auto const pLayerName = detail::nullSafeData(layerName);

  uint32_t extCount = 0;
  {
    auto const vkResult = VULKAN::vkEnumerateDeviceExtensionProperties(
        handle, pLayerName, &extCount, NULL);
    VX_VULKAN_SPEC(vkResult != VK_INCOMPLETE);
    if (vkResult != VK_SUCCESS) {
      return make_error_code(detail::fromVulkan(vkResult));
    }
  }

  std::vector<VULKAN::VkExtensionProperties> temp(extCount);
  {
    auto const vkResult = VULKAN::vkEnumerateDeviceExtensionProperties(
        handle, pLayerName, &extCount, temp.data());
    VX_VULKAN_SPEC(vkResult != VK_INCOMPLETE);
    if (vkResult != VK_SUCCESS) {
      return make_error_code(detail::fromVulkan(vkResult));
    }
  }

  std::vector<ExtensionProperties> retval(temp.size());
  for (size_t i = 0; i < temp.size(); ++i) {
    retval[i] = detail::fromVulkan(temp[i]);
  }
  return retval;
}

Result<std::vector<PhysicalDevice>>
vx::PhysicalDevice::enumerate(Instance const& instance)
{
  VX_REQUIRE(instance.isValid());

  uint32_t gpuCount = 0;
  {
    auto const vkResult =
        VULKAN::vkEnumeratePhysicalDevices(instance.handle, &gpuCount, NULL);
    VX_VULKAN_SPEC(vkResult != VK_INCOMPLETE);
    if (vkResult != VK_SUCCESS) {
      return make_error_code(detail::fromVulkan(vkResult));
    }
  }

  std::vector<VULKAN::VkPhysicalDevice> temp(gpuCount);
  {
    auto const vkResult = VULKAN::vkEnumeratePhysicalDevices(
        instance.handle, &gpuCount, temp.data());

    // VK_INCOMPLETE is not possible as enough memory is always provided.
    VX_VULKAN_SPEC(vkResult != VK_INCOMPLETE);

    if (vkResult != VK_SUCCESS) {
      return make_error_code(detail::fromVulkan(vkResult));
    }
  }

  std::vector<PhysicalDevice> retval(temp.size());
  for (size_t i = 0; i < temp.size(); ++i) {
    retval[i].handle = temp[i];
  }
  return retval;
}

bool
PhysicalDeviceInfo::isPreferable(
    PhysicalDeviceInfo const& lhs, PhysicalDeviceInfo const& rhs) noexcept
{
  auto const proj = [](PhysicalDeviceInfo const& info) {
    auto const first = [&] {
      switch (info.properties.deviceType) {
        case PhysicalDevice::Type::OTHER:
          return 1;
        case PhysicalDevice::Type::INTEGRATED_GPU:
          return 4;
        case PhysicalDevice::Type::DISCRETE_GPU:
          return 5;
        case PhysicalDevice::Type::VIRTUAL_GPU:
          return 3;
        case PhysicalDevice::Type::CPU:
          return 2;
      }

      VX_UNREACHABLE();
    }();

    auto const max      = std::numeric_limits<decltype(info.index)>::max();
    auto const invIndex = max - info.index;
    return std::tuple{first, info.properties.apiVersion, invIndex};
  };

  return proj(lhs) > proj(rhs);
}

Result<std::vector<PhysicalDeviceInfo>>
vx::PhysicalDeviceInfo::enumerate(
    Instance const& instance, Surface const& surface)
{
  VX_REQUIRE(instance.isValid());

  auto physDevs = VX_TRY(PhysicalDevice::enumerate(instance));

  std::vector<PhysicalDeviceInfo> retval;
  retval.reserve(physDevs.size());
  for (size_t i = 0; i < physDevs.size(); ++i) {
    auto& physDev = physDevs[i];
    VX_ASSERT(physDev.isValid());
    retval.push_back(
        {i,
         physDev,
         physDev.getProperties(),
         VX_TRY(physDev.getQueueFamilyProperties(surface)),
         VX_TRY(physDev.enumerateExtensionProperties())});
  }
  return retval;
}

Result<PhysicalDeviceInfo>
vx::PhysicalDeviceInfo::select(Instance const& instance, Surface const& surface)
{
  return PhysicalDeviceInfo::select(
      instance, surface, PhysicalDeviceInfo::isPreferable);
}

Result<uint32_t>
vx::selectSingleQueue(
    std::vector<PhysicalDevice::QueueFamilyProperties> const& qFamProps,
    Bitset<QueueFlagBits> queueFlags)
{
  struct Candidate {
    uint32_t invNumBits    = 0;
    uint32_t invQueueCount = 0;
    uint32_t index         = 0;

    auto operator<=>(Candidate const&) const = default;
  };

  std::vector<Candidate> compatible;
  compatible.reserve(qFamProps.size());

  for (auto const& x: qFamProps) {
    bool const xHasAllQueueFlag = ((queueFlags & x.queueFlags) == queueFlags);

    if (xHasAllQueueFlag) {
      uint32_t const max32 = std::numeric_limits<uint32_t>::max();
      compatible.push_back({
          .invNumBits = max32 - static_cast<uint32_t>(x.queueFlags.popcount()),
          .invQueueCount = max32 - x.queueCount,
          .index         = x.index,
      });
    }
  }

  if (compatible.empty()) {
    return make_error_code(StatusCode::ERROR_NO_MATCHING_QUEUE_FAMILY);
  }

  std::ranges::sort(compatible);

  return compatible[0].index;
}
