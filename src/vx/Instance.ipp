namespace vx::detail {
  inline LayerProperties fromVulkan(VULKAN::VkLayerProperties const& x)
  {
    return {
        .layerName             = x.layerName,
        .specVersion           = Version{.data = x.specVersion},
        .implementationVersion = Version{.data = x.implementationVersion},
        .description           = x.description,
    };
  }

  inline ExtensionProperties fromVulkan(VULKAN::VkExtensionProperties const& x)
  {
    return {
        .extensionName = x.extensionName,
        .specVersion   = Version{.data = x.specVersion},
    };
  }
}
