#include "Glfw.hpp"

#include <atomic>

using namespace vx;

namespace vx::detail {

  class GlfwErrorCodeCategory : public std::error_category {
   public:
    virtual char const* name() const noexcept override final
    {
      return "vx::GlfwErrorCode";
    }

    virtual std::string message(int const c) const noexcept override final
    {
      switch (static_cast<GlfwErrorCode>(c)) {
#define CASE(x)          \
  case GlfwErrorCode::x: \
    return #x;
        CASE(NO_ERROR)
        CASE(NOT_INITIALIZED)
        CASE(NO_CURRENT_CONTEXT)
        CASE(INVALID_ENUM)
        CASE(INVALID_VALUE)
        CASE(OUT_OF_MEMORY)
        CASE(API_UNAVAILABLE)
        CASE(VERSION_UNAVAILABLE)
        CASE(PLATFORM_ERROR)
        CASE(FORMAT_UNAVAILABLE)
        CASE(NO_WINDOW_CONTEXT)
        CASE(ALREADY_INITIALIZED)
        CASE(VULKAN_NOT_SUPPORTED)
#undef CASE
      }

      VX_UNREACHABLE();
    }

    virtual std::error_condition
    default_error_condition(int const c) const noexcept override final
    {
      if (static_cast<GlfwErrorCode>(c) == GlfwErrorCode::NO_ERROR) {
        return std::error_condition{};
      }
      else {
        return std::error_condition(c, *this);
      }
    }

    static GlfwErrorCodeCategory const& instance() noexcept
    {
      static GlfwErrorCodeCategory c;
      return c;
    }
  };
}

namespace vx {
  std::error_code make_error_code(GlfwErrorCode const c) noexcept
  {
    return {static_cast<int>(c), detail::GlfwErrorCodeCategory::instance()};
  }

  std::error_condition make_error_condition(GlfwErrorCode const c) noexcept
  {
    return {static_cast<int>(c), detail::GlfwErrorCodeCategory::instance()};
  }
}

namespace vx::detail {
  std::optional<Error> glfwMaybeError() noexcept
  {
    char const* errMsg   = nullptr;
    auto const glfwError = GLFW::glfwGetError(&errMsg);
    if (glfwError == GLFW_NO_ERROR) {
      return std::nullopt;
    }

    auto const ec = make_error_code(static_cast<GlfwErrorCode>(glfwError));
    if (errMsg != nullptr) {
      return ErrorMessage(ec, errMsg);
    }
    else {
      return ec;
    }
  }
}

namespace {
  bool isGlfwInitialized = false;
}

Result<Glfw>
Glfw::init()
{
  if (isGlfwInitialized) {
    return make_error_code(GlfwErrorCode::ALREADY_INITIALIZED);
  }

  {
    isGlfwInitialized  = true;
    auto const success = GLFW::glfwInit();
    if (success == GLFW_FALSE) {
      isGlfwInitialized = false;

      VX_GLFW_RETURN_ASSERT_ERROR();
    }
  }

  Glfw retval;
  retval.live = true;

  {
    auto const supp = GLFW::glfwVulkanSupported();
    VX_GLFW_RETURN_IF_ERROR();

    if (supp != GLFW_TRUE) {
      return make_error_code(GlfwErrorCode::VULKAN_NOT_SUPPORTED);
    }
  }

  return {std::move(retval)};
}

Glfw::Glfw(Glfw&& other) : live(other.live)
{
  other.live = false;
}

Glfw&
Glfw::operator=(Glfw&& other)
{
  live       = other.live;
  other.live = false;
  return (*this);
}

Glfw::~Glfw()
{
  if (live) {
    GLFW::glfwTerminate();

    VX_ASSERT(isGlfwInitialized);
    isGlfwInitialized = false;
  }
};

Result<void>
Glfw::pollEvents() noexcept
{
  GLFW::glfwPollEvents();
  VX_GLFW_RETURN_IF_ERROR();
  return {};
}

Result<void>
Glfw::waitEvents() noexcept
{
  GLFW::glfwWaitEvents();
  VX_GLFW_RETURN_IF_ERROR();
  return {};
}

Result<void>
Glfw::waitEventsTimeout(double const timeoutSeconds) noexcept
{
  GLFW::glfwWaitEventsTimeout(timeoutSeconds);
  VX_GLFW_RETURN_IF_ERROR();
  return {};
}

Result<void>
Glfw::postEmptyEvent() noexcept
{
  GLFW::glfwPostEmptyEvent();
  VX_GLFW_RETURN_IF_ERROR();
  return {};
}

Result<std::vector<std::string>>
Glfw::getRequiredInstanceExtensions() noexcept
{
  uint32_t count        = 0;
  char const** extNames = GLFW::glfwGetRequiredInstanceExtensions(&count);
  if (extNames == nullptr) {
    VX_GLFW_RETURN_ASSERT_ERROR();
  }

  std::vector<std::string> retval(count);
  for (uint32_t i = 0; i < count; ++i) {
    retval[i] = extNames[i];
  }
  return retval;
}

Result<bool>
Glfw::hasPresentationSupport(
    Instance const& instance,
    PhysicalDevice const& physDev,
    uint32_t const queueFamilyIndex)
{
  VX_REQUIRE(instance.isValid() && physDev.isValid());

  int const result = GLFW::glfwGetPhysicalDevicePresentationSupport(
      instance.handle, physDev.handle, queueFamilyIndex);
  VX_GLFW_RETURN_IF_ERROR();

  return detail::boolFromGlfw(result);
}
