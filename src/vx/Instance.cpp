#include "Instance.hpp"

#include "Detail.hpp"

using namespace vx;

Instance::Instance(Instance&& other)
  : handle(std::move(other.handle)), createInfo(std::move(other.createInfo))
{
  other.handle     = VK_NULL_HANDLE;
  other.createInfo = {};
}

Instance&
Instance::operator=(Instance&& other)
{
  handle     = std::move(other.handle);
  createInfo = std::move(other.createInfo);

  other.handle     = VK_NULL_HANDLE;
  other.createInfo = {};

  return (*this);
}

Instance::~Instance()
{
  destroy();
}

bool
Instance::isValid() const noexcept
{
  return (handle != VK_NULL_HANDLE);
}

void
Instance::destroy() noexcept
{
  if (isValid()) {
    vkDestroyInstance(handle, NULL);
  }
  handle     = VK_NULL_HANDLE;
  createInfo = {};
}

Result<Instance>
Instance::create(
    ApplicationInfo const& applicationInfo, CreateInfo createInfo) noexcept
{
  VULKAN::VkApplicationInfo const vkApplicationInfo{
      .sType              = VK_STRUCTURE_TYPE_APPLICATION_INFO,
      .pNext              = NULL,
      .pApplicationName   = applicationInfo.applicationName.c_str(),
      .applicationVersion = applicationInfo.applicationVersion.data,
      .pEngineName        = applicationInfo.engineName.c_str(),
      .engineVersion      = applicationInfo.engineVersion.data,
      .apiVersion         = applicationInfo.apiVersion.data,
  };

  auto const pEnabledLayerNames =
      detail::charPtrVector(createInfo.enabledLayerNames);
  auto const vkExtNames =
      detail::charPtrVector(createInfo.enabledExtensionNames);

  VULKAN::VkInstanceCreateInfo const vkCreateInfo{
      .sType                 = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
      .pNext                 = NULL,
      .flags                 = 0,
      .pApplicationInfo      = &vkApplicationInfo,
      .enabledLayerCount     = static_cast<uint32_t>(pEnabledLayerNames.size()),
      .ppEnabledLayerNames   = detail::nullSafeData(pEnabledLayerNames),
      .enabledExtensionCount = static_cast<uint32_t>(vkExtNames.size()),
      .ppEnabledExtensionNames = detail::nullSafeData(vkExtNames),
  };

  Instance retval;
  auto const vkResult = vkCreateInstance(&vkCreateInfo, NULL, &retval.handle);
  if (vkResult != VK_SUCCESS) {
    VX_ASSERT(retval.handle == VK_NULL_HANDLE);
    return make_error_code(detail::fromVulkan(vkResult));
  }
  retval.createInfo = std::move(createInfo);
  VX_ASSERT(retval.isValid());
  return {std::move(retval)};
}

Result<Version>
Instance::enumerateVersion() noexcept
{
  Version retval;
  auto const vkResult = VULKAN::vkEnumerateInstanceVersion(&retval.data);
  if (vkResult != VK_SUCCESS) {
    return make_error_code(detail::fromVulkan(vkResult));
  }
  return {std::move(retval)};
}

Result<std::vector<LayerProperties>>
Instance::enumerateLayerProperties() noexcept
{
  uint32_t layerCount = 0;
  {
    auto const vkResult =
        VULKAN::vkEnumerateInstanceLayerProperties(&layerCount, NULL);
    VX_VULKAN_SPEC(vkResult != VK_INCOMPLETE);
    if (vkResult != VK_SUCCESS) {
      return make_error_code(detail::fromVulkan(vkResult));
    }
  }

  std::vector<VULKAN::VkLayerProperties> temp(layerCount);
  {
    auto const vkResult =
        VULKAN::vkEnumerateInstanceLayerProperties(&layerCount, temp.data());
    VX_VULKAN_SPEC(vkResult != VK_INCOMPLETE);
    if (vkResult != VK_SUCCESS) {
      return make_error_code(detail::fromVulkan(vkResult));
    }
  }

  std::vector<LayerProperties> retval(temp.size());
  for (size_t i = 0; i < temp.size(); ++i) {
    retval[i] = detail::fromVulkan(temp[i]);
  }
  return retval;
}

Result<std::vector<ExtensionProperties>>
Instance::enumerateExtensionProperties(std::string const& layerName) noexcept
{
  auto const pLayerName = detail::nullSafeData(layerName);

  uint32_t extCount = 0;
  {
    auto const vkResult = VULKAN::vkEnumerateInstanceExtensionProperties(
        pLayerName, &extCount, NULL);
    VX_VULKAN_SPEC(vkResult != VK_INCOMPLETE);
    if (vkResult != VK_SUCCESS) {
      return make_error_code(detail::fromVulkan(vkResult));
    }
  }

  std::vector<VULKAN::VkExtensionProperties> temp(extCount);
  {
    auto const vkResult = VULKAN::vkEnumerateInstanceExtensionProperties(
        pLayerName, &extCount, temp.data());
    VX_VULKAN_SPEC(vkResult != VK_INCOMPLETE);
    if (vkResult != VK_SUCCESS) {
      return make_error_code(detail::fromVulkan(vkResult));
    }
  }

  std::vector<ExtensionProperties> retval(temp.size());
  for (size_t i = 0; i < temp.size(); ++i) {
    retval[i] = detail::fromVulkan(temp[i]);
  }
  return retval;
}
