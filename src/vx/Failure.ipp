namespace vx::detail {
  template<typename FormatString, typename... Args>
  void fmtHandler(
      char const* file,
      long const line,
      char const* func,
      FormatString const& format_string,
      Args const&... args)
  {
    fmt::print(
        stderr,
        FMT_STRING("vx: error in function {} at {}:{} : "),
        func,
        file,
        line);
    fmt::print(stderr, format_string, args...);
    fmt::print(stderr, "\n");
  }

  template<typename... Args>
  [[noreturn]] void maybeFmtFailureHandler(
      char const* file, long const line, char const* func, Args const&... args)
  {
    if constexpr (sizeof...(args) == 0) {
      fmtHandler(file, line, func, "unknown failure");
    }
    else {
      fmtHandler(file, line, func, args...);
    }
    runFailureHandler();
  }

  template<typename... Args>
  [[noreturn]] void maybeFmtAssertHandler(
      char const* file, long const line, char const* func, Args const&... args)
  {
    if constexpr (sizeof...(args) == 0) {
      fmtHandler(file, line, func, "unknown assertion");
    }
    else {
      fmtHandler(file, line, func, args...);
    }
    std::abort();
  }

  // Creates an automatic variables whose destructor calls *dtorCallback*. Then
  // calls the failure handler if *positive* is not positive.
  // Only used for unit tests!
  void unitTestFunction(int positive, void (*dtorCallback)());
}
