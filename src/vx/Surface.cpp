#include "Surface.hpp"

using namespace vx;

Surface::Surface(Surface&& other)
  : handle(std::move(other.handle)), instance(std::move(other.instance))
{
  other.handle   = VK_NULL_HANDLE;
  other.instance = VK_NULL_HANDLE;
}

Surface&
Surface::operator=(Surface&& other)
{
  handle         = std::move(other.handle);
  instance       = std::move(other.instance);
  other.handle   = VK_NULL_HANDLE;
  other.instance = VK_NULL_HANDLE;

  return (*this);
}

Surface::~Surface()
{
  destroy();
}

bool
Surface::isValid() const noexcept
{
  return (handle != VK_NULL_HANDLE);
}

void
Surface::destroy() noexcept
{
  if (isValid()) {
    VULKAN::vkDestroySurfaceKHR(instance, handle, NULL);
  }
  handle = VK_NULL_HANDLE;
}
