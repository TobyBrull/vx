namespace vx {
  namespace detail {
    template<typename T>
    struct VariantVisitor {
      template<typename S>
      std::variant<T, std::error_code, ErrorMessage, Consumed> operator()(S v)
      {
        return v;
      }
    };

    struct ErrorVisitor {
      template<typename T>
      Error operator()(T err) const noexcept
      {
        if constexpr (std::is_same_v<T, std::error_code>) {
          return err;
        }
        else if constexpr (std::is_same_v<T, ErrorMessage>) {
          return Error(std::move(err));
        }
        else {
          VX_UNREACHABLE();
        }
      }
    };

    struct ErrorCodeVisitor {
      template<typename T>
      std::error_code operator()(T const& err) const noexcept
      {
        if constexpr (std::is_same_v<T, std::error_code>) {
          return err;
        }
        else if constexpr (std::is_same_v<T, ErrorMessage>) {
          VX_ASSERT(err.data);
          return std::get<0>(*err.data);
        }
        else {
          VX_UNREACHABLE();
        }
      }
    };

    template<typename T>
    void handleUnconsumedResult(
        std::variant<T, std::error_code, ErrorMessage, Consumed> payload)
    {
      if (std::holds_alternative<Consumed>(payload)) {
        return;
      }

      if (std::holds_alternative<T>(payload)) {
        if constexpr (std::is_same_v<T, void>) {
          VX_ASSERT_FMT(false, "\nUnconsumed vx::Result<void> in value state");
        }
        // TODO: Uncomment once the next release of libfmt comes around.
        // else if constexpr (fmt::is_formattable<T>) {
        //  VX_ASSERT_FMT(
        //      false, "\nUnconsumed vx::Result, value = {}",
        //      std::move(payload));
        //}
        else {
          VX_ASSERT_FMT(false, "\nUnconsumed vx::Result, value = ???");
        }
      }
      else {
        VX_ASSERT(
            std::holds_alternative<std::error_code>(payload) &&
            std::holds_alternative<ErrorMessage>(payload));

        VX_ASSERT_FMT(
            false,
            "\nUnconsumed vx::Result, error = {}",
            std::visit(ErrorVisitor{}, std::move(payload)));
      }
    }
  }

  ///////////////////
  // Result<T>
  template<typename T>
  Result<T>::~Result()
  {
    detail::handleUnconsumedResult(std::move(payload));
  }

  template<typename T>
  Result<T>::Result(T value) noexcept(std::is_nothrow_move_constructible_v<T>)
    : payload(std::move(value))
  {
  }

  template<typename T>
  Result<T>::Result(std::error_code errCode) noexcept
    : payload(std::move(errCode))
  {
  }

  template<typename T>
  Result<T>::Result(ErrorMessage errMsg) noexcept : payload(std::move(errMsg))
  {
  }

  template<typename T>
  Result<T>::Result(Error error) noexcept
    : payload(std::visit(detail::VariantVisitor<T>{}, std::move(error.data)))
  {
  }

  template<typename T>
  bool Result<T>::isValue() const noexcept
  {
    return std::holds_alternative<T>(payload);
  }

  template<typename T>
  bool Result<T>::isError() const noexcept
  {
    return std::holds_alternative<std::error_code>(payload) ||
        std::holds_alternative<ErrorMessage>(payload);
  }

  template<typename T>
  T Result<T>::value() &&
  {
    VX_REQUIRE(isValue());
    auto retval = std::get<T>(std::move(payload));
    payload     = Consumed{};
    return retval;
  }

  template<typename T>
  Error Result<T>::error() &&
  {
    VX_REQUIRE(isError());
    auto retval = std::visit(detail::ErrorVisitor{}, std::move(payload));
    payload     = Consumed{};
    return retval;
  }

  template<typename T>
  void Result<T>::consume() noexcept
  {
    payload = Consumed{};
  }

  ///////////////////
  // Result<void>
  inline Result<void>::~Result()
  {
    detail::handleUnconsumedResult(std::move(payload));
  }

  inline Result<void>::Result(std::error_code error) noexcept
    : payload(std::move(error))
  {
  }

  inline Result<void>::Result(ErrorMessage errMsg) noexcept
    : payload(std::move(errMsg))
  {
  }

  inline Result<void>::Result(Error error) noexcept
    : payload(std::visit(
          detail::VariantVisitor<std::monostate>{}, std::move(error.data)))
  {
  }

  inline bool Result<void>::isValue() const noexcept
  {
    return std::holds_alternative<std::monostate>(payload);
  }

  inline bool Result<void>::isError() const noexcept
  {
    return std::holds_alternative<std::error_code>(payload) ||
        std::holds_alternative<ErrorMessage>(payload);
  }

  inline void Result<void>::value() &&
  {
    VX_REQUIRE(isValue());
    payload = Consumed{};
    return;
  }

  inline Error Result<void>::error() &&
  {
    VX_REQUIRE(isError());
    auto retval = std::visit(detail::ErrorVisitor{}, std::move(payload));
    payload     = Consumed{};
    return retval;
  }

  inline void Result<void>::consume() noexcept
  {
    payload = Consumed{};
  }
}

namespace vx::detail {
  template<typename T>
  struct isResultType : public std::false_type {
  };

  template<typename T>
  struct isResultType<Result<T>> : public std::true_type {
  };
}

namespace vx::detail {
  inline StatusCode fromVulkan(VULKAN::VkResult const vkResult) noexcept
  {
    return static_cast<StatusCode>(vkResult);
  }
}

namespace std {
  template<>
  struct is_error_condition_enum<vx::StatusCode> : public true_type {
  };
}

/// Supports the same format specifications as a string.
template<>
struct fmt::formatter<vx::Error> : public fmt::formatter<std::string_view> {
  template<typename FormatContext>
  auto format(vx::Error const& err, FormatContext& ctx)
  {
    struct Visitor {
      fmt::formatter<vx::Error>* self;
      FormatContext& ctx;

      auto operator()(std::error_code const ec) const
      {
        return self->fmt::formatter<std::string_view>::format(
            ec.message(), ctx);
      }
      auto operator()(vx::ErrorMessage const& em) const
      {
        VX_ASSERT(em.data);
        return self->fmt::formatter<std::string_view>::format(
            std::get<1>(*em.data), ctx);
      }
    };
    return std::visit(Visitor{.self = this, .ctx = ctx}, err.data);
  }
};
