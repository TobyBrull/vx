VX_DETAIL_ENUM_FORMATTER_DECLARE(vx::CommandPool::CreateInfo::FlagBits);
VX_DETAIL_ENUM_FORMATTER_DECLARE(vx::CommandBuffer::Level);

namespace vx::detail {
  inline VULKAN::VkCommandBufferLevel
  toVulkan(vx::CommandBuffer::Level const level)
  {
    return static_cast<VULKAN::VkCommandBufferLevel>(level);
  }
}
