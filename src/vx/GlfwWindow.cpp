#include "GlfwWindow.hpp"

using namespace vx;

GlfwWindow::GlfwWindow(GlfwWindow&& other) : handle(other.handle)
{
  other.handle = nullptr;
}

GlfwWindow&
GlfwWindow::operator=(GlfwWindow&& other)
{
  handle       = other.handle;
  other.handle = nullptr;
  return (*this);
}

GlfwWindow::~GlfwWindow()
{
  VX_TRY_IGNORE(destroy());
}

bool
GlfwWindow::isValid() const noexcept
{
  return (handle != nullptr);
}

Result<void>
GlfwWindow::destroy() noexcept
{
  if (isValid()) {
    GLFW::glfwDestroyWindow(handle);
    VX_GLFW_RETURN_IF_ERROR();
  }
  handle = nullptr;

  return {};
}

namespace {
  Result<void> setWindowHints(GlfwWindow::CreateInfo const& createInfo)
  {
    GLFW::glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    VX_GLFW_RETURN_IF_ERROR();

    GLFW::glfwWindowHint(
        GLFW_RESIZABLE, detail::boolToGlfw(createInfo.resizable));
    VX_GLFW_RETURN_IF_ERROR();

    return {};
  }
}

Result<GlfwWindow>
GlfwWindow::create(CreateInfo createInfo)
{
  VX_TRY(setWindowHints(createInfo));
  GLFW::GLFWwindow* handle = GLFW::glfwCreateWindow(
      createInfo.size.width,
      createInfo.size.height,
      createInfo.title.c_str(),
      nullptr,
      nullptr);
  if (handle == nullptr) {
    VX_GLFW_RETURN_ASSERT_ERROR();
  }

  GlfwWindow retval;
  retval.handle = handle;
  return {std::move(retval)};
}

Result<bool>
GlfwWindow::shouldClose() const
{
  VX_REQUIRE(isValid());

  auto const retval = GLFW::glfwWindowShouldClose(handle);
  VX_GLFW_RETURN_IF_ERROR();
  return {detail::boolFromGlfw(retval)};
}

Result<void>
GlfwWindow::shouldClose(bool const shouldClose) const
{
  VX_REQUIRE(isValid());

  GLFW::glfwSetWindowShouldClose(handle, detail::boolToGlfw(shouldClose));
  VX_GLFW_RETURN_IF_ERROR();
  return {};
}

Result<Extent2D>
GlfwWindow::framebufferSize() const
{
  VX_REQUIRE(isValid());

  int width  = 0;
  int height = 0;
  GLFW::glfwGetFramebufferSize(handle, &width, &height);
  if ((width == 0) && (height == 0)) {
    VX_GLFW_RETURN_IF_ERROR();
  }

  VX_ASSERT(width >= 0);
  VX_ASSERT(height >= 0);

  return Extent2D{
      .width  = static_cast<uint32_t>(width),
      .height = static_cast<uint32_t>(height),
  };
}

Result<Surface>
GlfwWindow::createSurface(Instance const& instance) const
{
  VX_REQUIRE(isValid() && instance.isValid());

  VULKAN::VkSurfaceKHR surface = VK_NULL_HANDLE;
  VULKAN::VkResult const result =
      GLFW::glfwCreateWindowSurface(instance.handle, handle, NULL, &surface);
  if (result != VK_SUCCESS) {
    VX_ASSERT(surface == VK_NULL_HANDLE);
    return make_error_code(detail::fromVulkan(result));
  }
  VX_ASSERT(surface != VK_NULL_HANDLE);

  Surface retval;
  retval.handle   = surface;
  retval.instance = instance.handle;
  return {std::move(retval)};
}
