#ifndef __INCLUDE_GUARD_VX_INSTANCE_HPP
#define __INCLUDE_GUARD_VX_INSTANCE_HPP

#include "Result.hpp"
#include "Version.hpp"
#include "Vulkan.hpp"

namespace vx {
  /// \rst
  /// Contains information that the Vulkan implementation can use to refer to a
  /// specific application.
  ///
  /// :vkspec:`chap5.html#VkApplicationInfo`
  /// \endrst
  struct ApplicationInfo {
    std::string applicationName;
    Version applicationVersion;

    std::string engineName;
    Version engineVersion;

    Version apiVersion = Version::create(1);
  };

  /// \rst
  /// :vkspec:`chap39.html#VkLayerProperties`
  /// \endrst
  struct LayerProperties {
    std::string layerName;
    Version specVersion;
    Version implementationVersion;
    std::string description;
  };

  /// \rst
  /// :vkspec:`chap39.html#VkExtensionProperties`
  /// \endrst
  struct ExtensionProperties {
    std::string extensionName;
    Version specVersion;
  };

  /// \rst
  /// An instance is how an application connects to the Vulkan implementation.
  ///
  /// Instances are moveable, but not copyable, and have a non-trivial
  /// destructor.
  ///
  /// :vkspec:`chap5.html#initialization-instances`.
  /// \endrst
  struct Instance {
    /// The Vulkan handle.
    VULKAN::VkInstance handle = VK_NULL_HANDLE;

    /// \rst
    /// :vkspec:`chap5.html#VkInstanceCreateInfo`
    /// \endrst
    struct CreateInfo {
      std::vector<std::string> enabledLayerNames;
      std::vector<std::string> enabledExtensionNames;

      /// Defaulted three-way comparison.
      auto operator<=>(CreateInfo const&) const noexcept = default;
    };

    /// \rst
    /// The :cpp:struct:`~vx::Instance::CreateInfo` used to
    /// :cpp:func:`~vx::Instance::create()` this :cpp:struct:`~vx::Instance`.
    /// \endrst
    CreateInfo createInfo;

    Instance() = default;

    Instance(Instance&&);
    Instance& operator=(Instance&&);

    ~Instance();

    /// \rst
    /// Whether the :cpp:member:`~vx::Instance::handle` is
    /// :code:`VK_NULL_HANDLE`.
    /// \endrst
    bool isValid() const noexcept;

    /// \rst
    /// :vkspec:`chap5.html#vkDestroyInstance`
    /// \endrst
    void destroy() noexcept;

    /// \rst
    /// :vkspec:`chap5.html#vkCreateInstance`
    /// \endrst
    static Result<Instance>
    create(ApplicationInfo const& = {}, CreateInfo = {}) noexcept;

    /// \rst
    /// :vkspec:`chap4.html#vkEnumerateInstanceVersion`
    /// \endrst
    static Result<Version> enumerateVersion() noexcept;

    /// \rst
    /// :vkspec:`chap39.html#vkEnumerateInstanceLayerProperties`
    /// \endrst
    static Result<std::vector<LayerProperties>>
    enumerateLayerProperties() noexcept;

    /// \rst
    /// :vkspec:`chap39.html#vkEnumerateInstanceExtensionProperties`
    ///
    /// Enumerates the :cpp:struct:`~vx::ExtensionProperties`, either for the
    /// layer with the given *layerName* or, if *layerName* is empty, for the
    /// whole instance.
    /// \endrst
    static Result<std::vector<ExtensionProperties>>
    enumerateExtensionProperties(std::string const& layerName = "") noexcept;
  };
}

#include "Instance.ipp"

#endif
