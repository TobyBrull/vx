namespace vx::detail {

  // :glfw:`group__init.html` :code:`glfwGetError`.
  std::optional<Error> glfwMaybeError() noexcept;

#define VX_GLFW_RETURN_IF_ERROR()        \
  {                                      \
    auto err = detail::glfwMaybeError(); \
    if (err.has_value()) {               \
      return std::move(err).value();     \
    }                                    \
  }

#define VX_GLFW_RETURN_ASSERT_ERROR()    \
  {                                      \
    auto err = detail::glfwMaybeError(); \
    VX_ASSERT(err.has_value());          \
    return std::move(err).value();       \
  }

  inline int boolToGlfw(bool const x)
  {
    if (x) {
      return GLFW_TRUE;
    }
    else {
      return GLFW_FALSE;
    }
  }

  inline bool boolFromGlfw(int const x)
  {
    if (x == GLFW_TRUE) {
      return true;
    }
    else {
      return false;
    }
  }
}

namespace std {
  template<>
  struct is_error_condition_enum<vx::GlfwErrorCode> : public true_type {
  };
}
