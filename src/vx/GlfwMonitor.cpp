#include "GlfwMonitor.hpp"

using namespace vx;

std::partial_ordering
GlfwVideoMode::operator<=>(GlfwVideoMode const& other) const noexcept
{
  auto const key = [](GlfwVideoMode const& x) {
    return std::tuple{
        x.redBits + x.greenBits + x.blueBits,
        static_cast<uint64_t>(x.width) * static_cast<uint64_t>(x.height)};
  };
  return key(*this) <=> key(other);
}

bool
GlfwMonitor::isValid() const noexcept
{
  return (handle != nullptr);
}

Result<GlfwMonitor>
GlfwMonitor::getPrimary() noexcept
{
  GLFW::GLFWmonitor* const handle = GLFW::glfwGetPrimaryMonitor();
  if (handle == nullptr) {
    VX_GLFW_RETURN_IF_ERROR();
  }

  return {GlfwMonitor{.handle = handle}};
}

Result<std::vector<GlfwMonitor>>
GlfwMonitor::enumerate() noexcept
{
  int count                         = 0;
  GLFW::GLFWmonitor** const handles = GLFW::glfwGetMonitors(&count);
  if (handles == nullptr) {
    VX_GLFW_RETURN_IF_ERROR();
  }
  VX_ASSERT((count == 0) || (handles != nullptr));

  std::vector<GlfwMonitor> retval(count);
  for (int i = 0; i < count; ++i) {
    retval[i].handle = handles[i];
  }
  return retval;
}

namespace vx::detail {
  GlfwVideoMode fromGlfw(GLFW::GLFWvidmode const& vm)
  {
    VX_ASSERT(vm.width >= 0);
    VX_ASSERT(vm.height >= 0);
    VX_ASSERT(vm.redBits >= 0);
    VX_ASSERT(vm.greenBits >= 0);
    VX_ASSERT(vm.blueBits >= 0);
    VX_ASSERT(vm.refreshRate >= 0);
    return GlfwVideoMode{
        .width       = static_cast<uint32_t>(vm.width),
        .height      = static_cast<uint32_t>(vm.height),
        .redBits     = static_cast<uint32_t>(vm.redBits),
        .greenBits   = static_cast<uint32_t>(vm.greenBits),
        .blueBits    = static_cast<uint32_t>(vm.blueBits),
        .refreshRate = static_cast<uint32_t>(vm.refreshRate),
    };
  }
}

Result<GlfwMonitor::Info>
GlfwMonitor::getInfo() const
{
  VX_REQUIRE(isValid());

  Info retval;
  {
    int xpos = 0;
    int ypos = 0;
    GLFW::glfwGetMonitorPos(handle, &xpos, &ypos);
    VX_GLFW_RETURN_IF_ERROR();

    retval.pos = {Offset2D{
        .x = xpos,
        .y = ypos,
    }};
  }

  {
    int xpos   = 0;
    int ypos   = 0;
    int width  = 0;
    int height = 0;
    GLFW::glfwGetMonitorWorkarea(handle, &xpos, &ypos, &width, &height);
    VX_GLFW_RETURN_IF_ERROR();

    VX_ASSERT(width >= 0);
    VX_ASSERT(height >= 0);

    retval.workarea = {Rect2D{
        .offset =
            Offset2D{
                .x = xpos,
                .y = ypos,
            },
        .extent =
            Extent2D{
                .width  = static_cast<uint32_t>(width),
                .height = static_cast<uint32_t>(height),
            },
    }};
  }

  {
    int width  = 0;
    int height = 0;
    GLFW::glfwGetMonitorPhysicalSize(handle, &width, &height);
    VX_GLFW_RETURN_IF_ERROR();

    VX_ASSERT(width >= 0);
    VX_ASSERT(height >= 0);

    retval.physicalSizeMM = {Extent2D{
        .width  = static_cast<uint32_t>(width),
        .height = static_cast<uint32_t>(height),
    }};
  }

  {
    float xscale = 0.0;
    float yscale = 0.0;
    GLFW::glfwGetMonitorContentScale(handle, &xscale, &yscale);
    VX_GLFW_RETURN_IF_ERROR();

    retval.contentScaleX = xscale;
    retval.contentScaleY = yscale;
  }

  {
    char const* pName = GLFW::glfwGetMonitorName(handle);
    VX_GLFW_RETURN_IF_ERROR();

    VX_ASSERT(pName != nullptr);

    retval.name = pName;
  }

  {
    int count                         = 0;
    GLFW::GLFWvidmode const* vidmodes = GLFW::glfwGetVideoModes(handle, &count);
    if (vidmodes == nullptr) {
      VX_GLFW_RETURN_ASSERT_ERROR();
    }

    retval.availableVideoModes.resize(count);
    for (int i = 0; i < count; ++i) {
      retval.availableVideoModes[i] = detail::fromGlfw(vidmodes[i]);
    }
  }

  {
    GLFW::GLFWvidmode const* vidmode = GLFW::glfwGetVideoMode(handle);
    if (vidmode == nullptr) {
      VX_GLFW_RETURN_ASSERT_ERROR();
    }

    retval.currentVideoMode = detail::fromGlfw(*vidmode);
  }

  return retval;
}
