namespace vx {
  template<typename EnumT>
  std::vector<EnumT> const& enumValues()
  {
    static auto values = [] {
      auto const& map = detail::EnumFormatter<EnumT>::enumMap;
      std::vector<EnumT> retval;
      retval.reserve(map.size());
      for (auto const& [key, val]: map) {
        retval.push_back(key);
      }
      return retval;
    }();
    return values;
  }

  template<typename EnumT>
  bool areFlagBits()
  {
    using UnderlyingType = std::underlying_type_t<EnumT>;
    static_assert(std::is_unsigned_v<UnderlyingType>);
    return std::ranges::all_of(enumValues<EnumT>(), [](auto const e) {
      return std::popcount(static_cast<UnderlyingType>(e)) == 1;
    });
  }

  template<typename EnumT>
  template<typename... EnumTs>
  constexpr Bitset<EnumT> Bitset<EnumT>::create(EnumT e, EnumTs... es) noexcept
  {
    static_assert((std::is_same_v<EnumT, EnumTs> && ...));

    Bitset<EnumT> retval{.data = static_cast<UnderlyingType>(e)};

    for (auto const x: std::array<EnumT, sizeof...(EnumTs)>{es...}) {
      retval.data |= static_cast<UnderlyingType>(x);
    }
    return retval;
  }

  template<typename EnumT>
  constexpr bool Bitset<EnumT>::test(EnumT const e) const noexcept
  {
    UnderlyingType const result = this->data & static_cast<UnderlyingType>(e);
    return result != 0;
  }

  template<typename EnumT>
  constexpr bool Bitset<EnumT>::any() const noexcept
  {
    return (data != 0);
  }

  template<typename EnumT>
  constexpr bool Bitset<EnumT>::none() const noexcept
  {
    return (data == 0);
  }

  template<typename EnumT>
  constexpr Bitset<EnumT>
  Bitset<EnumT>::operator&(Bitset<EnumT> const& other) const noexcept
  {
    UnderlyingType const result = this->data & other.data;
    return {.data = result};
  }

  template<typename EnumT>
  constexpr Bitset<EnumT>
  Bitset<EnumT>::operator|(Bitset<EnumT> const& other) const noexcept
  {
    UnderlyingType const result = this->data | other.data;
    return {.data = result};
  }

  template<typename EnumT>
  constexpr Bitset<EnumT> Bitset<EnumT>::operator|(EnumT const e) const noexcept
  {
    UnderlyingType const result = this->data | static_cast<UnderlyingType>(e);
    return {.data = result};
  }

  template<typename EnumT>
  constexpr int Bitset<EnumT>::popcount() const noexcept
  {
    return std::popcount(data);
  }

  template<typename EnumT, typename... EnumTs>
  constexpr Bitset<EnumT> createBitset(EnumT e, EnumTs... es) noexcept
  {
    return Bitset<EnumT>::create(e, es...);
  }
}
