namespace vx {
  inline uint32_t Version::major() const noexcept
  {
    return VK_VERSION_MAJOR(data);
  }
  inline uint32_t Version::minor() const noexcept
  {
    return VK_VERSION_MINOR(data);
  }
  inline uint32_t Version::patch() const noexcept
  {
    return VK_VERSION_PATCH(data);
  }

  inline Version Version::create(
      uint32_t const major, uint32_t const minor, uint32_t const patch)
  {
    VX_REQUIRE_FMT(major >> 10 == 0, "major = {} is too large", major);
    VX_REQUIRE_FMT(minor >> 10 == 0, "minor = {} is too large", minor);
    VX_REQUIRE_FMT(patch >> 12 == 0, "patch = {} is too large", patch);
    Version retval;
    retval.data = VK_MAKE_VERSION(major, minor, patch);
    return retval;
  }
}

/// \rst
/// Versions can be formatted as shown in the :cpp:class:`class description
/// <vx::Version>`.
///
/// Supports the same format specifications as a string.
/// \endrst
template<>
struct fmt::formatter<vx::Version> : public fmt::formatter<std::string_view> {
  template<typename FormatContext>
  auto format(vx::Version const& v, FormatContext& ctx)
  {
    std::array const vv = {v.major(), v.minor(), v.patch()};
    fmt::memory_buffer out;
    fmt::format_to(out, "{}", fmt::join(vv, "."));
    return fmt::formatter<std::string_view>::format(
        std::string_view(out.data(), out.size()), ctx);
  }
};
