#ifndef __INCLUDE_GUARD_VX_GLFW_WINDOW_HPP
#define __INCLUDE_GUARD_VX_GLFW_WINDOW_HPP

#include "GlfwMonitor.hpp"
#include "Surface.hpp"

#include <memory>

namespace vx {
  /// \rst
  /// Represents a window.
  ///
  /// GlfwWindows are moveable, but not copyable.
  ///
  /// This object may only be used from the main thread.
  /// \endrst
  struct GlfwWindow {
    GLFW::GLFWwindow* handle = nullptr;

    struct CreateInfo {
      Extent2D size = {
          .width  = 640,
          .height = 480,
      };
      bool resizable = true;
      std::string title;

      auto operator<=>(CreateInfo const&) const noexcept = default;
    };

    GlfwWindow() = default;

    GlfwWindow(GlfwWindow&&);
    GlfwWindow& operator=(GlfwWindow&&);

    ~GlfwWindow();

    /// \rst
    /// Whether the :cpp:member:`~vx::GlfwWindow::handle` is :code:`nullptr`.
    /// \endrst
    bool isValid() const noexcept;

    /// \rst
    /// :glfw:`group__window.html` :code:`glfwDestroyWindow`.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`~vx::GlfwWindow::isValid()`.
    /// \endrst
    Result<void> destroy() noexcept;

    /// \rst
    /// :glfw:`group__window.html` :code:`glfwCreateWindow`.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`~vx::GlfwWindow::isValid()`.
    /// \endrst
    static Result<GlfwWindow> create(CreateInfo);

    /// \rst
    /// :glfw:`group__window.html` :code:`glfwCreateWindow`.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`~vx::GlfwWindow::isValid()`.
    /// \endrst
    static Result<GlfwWindow> createFullscreen(GlfwMonitor const&, CreateInfo);

    /// \rst
    /// :glfw:`group__window.html` :code:`glfwWindowShouldClose`.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`~vx::GlfwWindow::isValid()`.
    /// \endrst
    Result<bool> shouldClose() const;

    /// \rst
    /// :glfw:`group__window.html` :code:`glfwSetWindowShouldClose`.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`~vx::GlfwWindow::isValid()`.
    /// \endrst
    Result<void> shouldClose(bool) const;

    /// \rst
    /// :glfw:`group_window.html` :code:`glfwGetFramebufferSize`.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`~vx::GlfwWindow::isValid()`.
    /// \endrst
    Result<Extent2D> framebufferSize() const;

    /// glfwSetWindowCloseCallback
    ///
    /// glfwGetWindowSize
    /// glfwSetWindowSize
    /// glfwSetWindowSizeCallback
    ///
    /// N/A
    /// glfwSetFramebufferSizeCallback
    ///
    /// glfwGetWindowPos
    /// glfwSetWindowPos
    /// glfwSetWindowPosCallback
    ///
    /// glfwGetWindowAttrib(..., GLFW_ICONIFIED)
    /// glfwGetWindowAttrib(..., GLFW_MAXIMIZED)
    /// glfwIconifyWindow
    /// glfwMaximizeWindow
    /// glfwRestoreWindow
    /// glfwSetWindowIconifyCallback
    /// glfwSetWindowMaximizeCallback
    /// glfwWindowHint(GLFW_ICONIFIED, ...)
    /// glfwWindowHint(GLFW_MAXIMIZED, ...)
    ///
    /// glfwSetWindowRefreshCallback
    ///
    /// glfwHideWindow
    /// glfwShowWindow
    /// glfwGetWindowAttrib(..., GLFW_VISIBLE)
    /// glfwWindowHint(GLFW_VISIBLE, ...)
    ///
    /// glfwFocusWindow
    /// glfwSetWindowFocusCallback
    /// glfwGetWindowAttrib(..., GLFW_FOCUSED)
    /// glfwWindowHint(GLFW_FOCUSED, ...)
    ///
    /// glfwRequestWindowAttention
    ///
    /// Attributes:
    /// glfwSetWindowAttrib
    ///
    ///   * GLFW_DECORATED
    ///   * GLFW_RESIZABLE
    ///   * GLFW_FLOATING
    ///   * GLFW_AUTO_ICONIFY
    ///   * GLFW_FOCUS_ON_SHOW
    ///
    /// glfwGetWindowAttrib
    ///
    ///   * the above
    ///   * GLFW_FOCUSED
    ///   * GLFW_ICONIFIED
    ///   * GLFW_MAXIMIZED
    ///   - GLFW_HOVERED
    ///   * GLFW_VISIBLE
    ///   *
    ///
    /// Simple Set Only:
    /// glfwSetWindowSizeLimits
    /// glfwSetWindowAspectRatio
    /// glfwSetWindowTitle
    /// glfwSetWindowIcon
    /// glfwSetWindowMonitor

    /// \rst
    /// :glfw:`group__vulkan.html` :code:`glfwCreateWindowSurface`
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`~vx::GlfwWindow::isValid()` or if not
    ///   :cpp:func:`vx::Instance::isValid()`.
    /// \endrst
    Result<Surface> createSurface(Instance const&) const;
  };
}

#endif
