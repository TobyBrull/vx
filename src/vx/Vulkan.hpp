#ifndef __INCLUDE_GUARD_VX_VULKAN_HPP
#define __INCLUDE_GUARD_VX_VULKAN_HPP

#include "Failure.hpp"
#include "Struct.hpp"

/// The main import of the Vulkan API.
#include <vulkan/vulkan.h>

#include <fmt/format.h>

#include <array>

/// \rst
/// A macro that is defined to be sustituted for nothing. Only used to
/// explicitly mark names from the actual Vulkan API, e.g., as in
/// :code:`VULKAN::VkInstance`.
/// \endrst
#define VULKAN

namespace vx {
  /// \rst
  /// Universal unique identifiers used by Vulkan.
  /// \endrst
  struct UUID {
    constexpr static int size      = 16;
    std::array<uint8_t, size> data = {};

    constexpr auto operator<=>(UUID const&) const noexcept = default;
  };

  /// \rst
  /// :vkspec:`chap4.html#VkExtent2D`
  ///
  /// Supports formatting via :cpp:struct:`vx::detail::StructFormatter`
  /// \endrst
  struct Extent2D {
    uint32_t width  = 0;
    uint32_t height = 0;

    constexpr auto operator<=>(Extent2D const&) const noexcept = default;
  };

  /// \rst
  /// :vkspec:`chap4.html#VkExtent3D`
  ///
  /// Supports formatting via :cpp:struct:`vx::detail::StructFormatter`
  /// \endrst
  struct Extent3D {
    uint32_t width  = 0;
    uint32_t height = 0;
    uint32_t depth  = 0;

    constexpr auto operator<=>(Extent3D const&) const noexcept = default;
  };

  /// \rst
  /// :vkspec:`chap4.html#VkOffset2D`
  ///
  /// Supports formatting via :cpp:struct:`vx::detail::StructFormatter`
  /// \endrst
  struct Offset2D {
    int32_t x = 0;
    int32_t y = 0;

    constexpr auto operator<=>(Offset2D const&) const noexcept = default;
  };

  /// \rst
  /// :vkspec:`chap4.html#VkOffset3D`
  ///
  /// Supports formatting via :cpp:struct:`vx::detail::StructFormatter`
  /// \endrst
  struct Offset3D {
    int32_t x = 0;
    int32_t y = 0;
    int32_t z = 0;

    constexpr auto operator<=>(Offset3D const&) const noexcept = default;
  };

  /// \rst
  /// :vkspec:`chap4.html#VkRect2D`
  ///
  /// Supports formatting via :cpp:struct:`vx::detail::StructFormatter`
  /// \endrst
  struct Rect2D {
    Offset2D offset;
    Extent2D extent;

    constexpr auto operator<=>(Rect2D const&) const noexcept = default;
  };
}

#include "Vulkan.ipp"

#endif
