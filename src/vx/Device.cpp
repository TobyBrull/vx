#include "Device.hpp"

#include "Detail.hpp"

#include <bit>

using namespace vx;

VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(Device::QueueCreateInfo::FlagBits) = {
    {Device::QueueCreateInfo::FlagBits::PROTECTED, "PROTECTED"},
};

Device::Device(Device&& other)
  : handle(std::move(other.handle)), createInfo(std::move(other.createInfo))
{
  other.handle     = VK_NULL_HANDLE;
  other.createInfo = {};
}

Device&
Device::operator=(Device&& other)
{
  handle     = std::move(other.handle);
  createInfo = std::move(other.createInfo);

  other.handle     = VK_NULL_HANDLE;
  other.createInfo = {};

  return (*this);
}

Device::~Device()
{
  destroy();
}

bool
Device::isValid() const noexcept
{
  return (handle != VK_NULL_HANDLE);
}

void
Device::destroy() noexcept
{
  if (isValid()) {
    vkDestroyDevice(handle, NULL);
  }
  handle     = VK_NULL_HANDLE;
  createInfo = {};
}

Result<Device>
Device::create(PhysicalDevice const& physDev, CreateInfo createInfo)
{
  VX_REQUIRE(physDev.isValid());

  auto const vkQueueCreateInfos = [&] {
    std::vector<VULKAN::VkDeviceQueueCreateInfo> retval;
    retval.reserve(createInfo.queueCreateInfos.size());
    for (auto const& info: createInfo.queueCreateInfos) {
      retval.push_back({
          .sType            = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
          .pNext            = NULL,
          .flags            = info.flags.data,
          .queueFamilyIndex = info.queueFamilyIndex,
          .queueCount = static_cast<uint32_t>(info.queuePriorities.size()),
          .pQueuePriorities = detail::nullSafeData(info.queuePriorities),
      });
    }
    return retval;
  }();

  auto const vkExtNames =
      detail::charPtrVector(createInfo.enabledExtensionNames);

  VULKAN::VkDeviceCreateInfo const vkCreateInfo{
      .sType                 = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
      .pNext                 = NULL,
      .flags                 = 0,
      .queueCreateInfoCount  = static_cast<uint32_t>(vkQueueCreateInfos.size()),
      .pQueueCreateInfos     = detail::nullSafeData(vkQueueCreateInfos),
      .enabledLayerCount     = 0,
      .ppEnabledLayerNames   = NULL,
      .enabledExtensionCount = static_cast<uint32_t>(vkExtNames.size()),
      .ppEnabledExtensionNames = detail::nullSafeData(vkExtNames),
      .pEnabledFeatures        = NULL,
  };

  Device retval;
  auto const vkResult = VULKAN::vkCreateDevice(
      physDev.handle, &vkCreateInfo, NULL, &retval.handle);
  if (vkResult != VK_SUCCESS) {
    VX_ASSERT(retval.handle == VK_NULL_HANDLE);
    return make_error_code(detail::fromVulkan(vkResult));
  }
  retval.createInfo = std::move(createInfo);
  VX_ASSERT(retval.isValid());
  return {std::move(retval)};
}

Result<Device>
Device::createWithSingleQueue(
    PhysicalDevice const& physDev,
    uint32_t const queueFamilyIndex,
    std::vector<std::string> enabledExtensionNames)
{
  Device::CreateInfo createInfo{
      .queueCreateInfos      = {{
          .queueFamilyIndex = queueFamilyIndex,
          .queuePriorities  = {1.0},
      }},
      .enabledExtensionNames = std::move(enabledExtensionNames),
  };
  return Device::create(physDev, std::move(createInfo));
}
