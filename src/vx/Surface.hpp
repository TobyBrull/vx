#ifndef __INCLUDE_GUARD_VX_SURFACE_HPP
#define __INCLUDE_GUARD_VX_SURFACE_HPP

#include "Vulkan.hpp"

namespace vx {

  /// \rst
  /// :vkspec:`chap33.html#VkSurfaceKHR`
  /// \endrst
  struct Surface {
    /// The Vulkan handle.
    VULKAN::VkSurfaceKHR handle = VK_NULL_HANDLE;

    /// \rst
    /// Instance for which this Surface was created.
    /// \endrst
    VULKAN::VkInstance instance = VK_NULL_HANDLE;

    Surface() = default;

    Surface(Surface&&);
    Surface& operator=(Surface&&);

    ~Surface();

    /// \rst
    /// Whether the :cpp:member:`~vx::Surface::handle` is
    /// :code:`VK_NULL_HANDLE`.
    /// \endrst
    bool isValid() const noexcept;

    /// \rst
    /// :vkspec:`chap33.html#vkDestroySurfaceKHR`
    /// \endrst
    void destroy() noexcept;
  };
}

#endif
