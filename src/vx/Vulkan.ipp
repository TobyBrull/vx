namespace vx::detail {
  inline VULKAN::VkExtent2D toVulkan(Extent2D const& x) noexcept
  {
    return {.width = x.width, .height = x.height};
  }
  inline Extent2D fromVulkan(VULKAN::VkExtent2D const& x) noexcept
  {
    return {.width = x.width, .height = x.height};
  }

  inline VULKAN::VkExtent3D toVulkan(Extent3D const& x) noexcept
  {
    return {.width = x.width, .height = x.height, .depth = x.depth};
  }
  inline Extent3D fromVulkan(VULKAN::VkExtent3D const& x) noexcept
  {
    return {.width = x.width, .height = x.height, .depth = x.depth};
  }

  inline VULKAN::VkOffset2D toVulkan(Offset2D const& x) noexcept
  {
    return {.x = x.x, .y = x.y};
  }
  inline Offset2D fromVulkan(VULKAN::VkOffset2D const& x) noexcept
  {
    return {.x = x.x, .y = x.y};
  }

  inline VULKAN::VkOffset3D toVulkan(Offset3D const& x) noexcept
  {
    return {.x = x.x, .y = x.y, .z = x.z};
  }
  inline Offset3D fromVulkan(VULKAN::VkOffset3D const& x) noexcept
  {
    return {.x = x.x, .y = x.y, .z = x.z};
  }

  inline VULKAN::VkRect2D toVulkan(Rect2D const& x) noexcept
  {
    return {.offset = toVulkan(x.offset), .extent = toVulkan(x.extent)};
  }
  inline Rect2D fromVulkan(VULKAN::VkRect2D const& x) noexcept
  {
    return {.offset = fromVulkan(x.offset), .extent = fromVulkan(x.extent)};
  }
}

/// \rst
/// Supports the same format specifications as a string. Prints the UUID in
/// hexadecimal form.
/// \endrst
template<>
struct fmt::formatter<vx::UUID> : public fmt::formatter<std::string_view> {
  template<typename FormatContext>
  auto format(vx::UUID const& uuid, FormatContext& ctx)
  {
    fmt::memory_buffer out;
    fmt::format_to(out, "{:02x}", fmt::join(uuid.data, ""));
    return fmt::formatter<std::string_view>::format(
        std::string_view(out.data(), out.size()), ctx);
  }
};

VX_DETAIL_STRUCT_FORMATTER(vx::Extent2D, uint32_t)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(width)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(height)
VX_DETAIL_STRUCT_FORMATTER_END()

VX_DETAIL_STRUCT_FORMATTER(vx::Extent3D, uint32_t)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(width)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(height)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(depth)
VX_DETAIL_STRUCT_FORMATTER_END()

VX_DETAIL_STRUCT_FORMATTER(vx::Offset2D, int32_t)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(x)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(y)
VX_DETAIL_STRUCT_FORMATTER_END()

VX_DETAIL_STRUCT_FORMATTER(vx::Offset3D, int32_t)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(x)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(y)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(z)
VX_DETAIL_STRUCT_FORMATTER_END()

VX_DETAIL_STRUCT_FORMATTER(vx::Rect2D, int32_t)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(offset.x)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(offset.y)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(extent.width)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(extent.height)
VX_DETAIL_STRUCT_FORMATTER_END()
