namespace vx::detail {
  // Returns a vector of char const* from a vector of strings.
  // WARNING: The returned vector may only be used as long as the given *v*
  // stays valid.
  std::vector<char const*> charPtrVector(std::vector<std::string> const& v);

  // Returns v.data(), but guaranteed to be the nullptr if v.empty().
  template<typename T>
  T const* nullSafeData(std::vector<T> const& v)
  {
    if (v.empty()) {
      return nullptr;
    }
    else {
      return v.data();
    }
  }

  inline char const* nullSafeData(std::string const& s)
  {
    if (s.empty()) {
      return nullptr;
    }
    else {
      return s.data();
    }
  }
}
