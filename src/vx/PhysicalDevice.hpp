#ifndef __INCLUDE_GUARD_VX_PHYSICAL_DEVICE_HPP
#define __INCLUDE_GUARD_VX_PHYSICAL_DEVICE_HPP

#include "Enum.hpp"

#include "Bitset.hpp"
#include "Instance.hpp"
#include "Surface.hpp"

namespace vx {

  /// \rst
  /// :vkspec:`chap6.html#VkQueueFlagBits`
  /// \endrst
  enum class QueueFlagBits : uint32_t {
    GRAPHICS       = 0x00000001,
    COMPUTE        = 0x00000002,
    TRANSFER       = 0x00000004,
    SPARSE_BINDING = 0x00000008,
    PROTECTED      = 0x00000010,

    /// \rst
    /// Represents the result of
    /// :vkspec:`chap33.html#vkGetPhysicalDeviceSurfaceSupportKHR`. This bit not
    /// being set can either mean that the corresponding extension
    /// ("VK_KHR_surface") was not enabled, or that the queue does not have
    /// surface support.
    ///
    /// vx add-on.
    /// \endrst
    SURFACE_SUPPORT = 0x10000001,
  };

  /// \rst
  /// Represents a physical device available to an :cpp:struct:`~vx::Instance`.
  ///
  /// PhysicalDevices are copyable and have a trivial destructor.
  ///
  /// :vkspec:`chap6.html#devsandqueues-physical-device-enumeration`
  /// \endrst
  struct PhysicalDevice {
    VULKAN::VkPhysicalDevice handle = VK_NULL_HANDLE;

    /// \rst
    /// If data is not equal to :code:`VK_NULL_HANDLE`.
    /// \endrst
    bool isValid() const noexcept;

    /// \rst
    /// :vkspec:`chap6.html#vkEnumeratePhysicalDevices`
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::Instance::isValid()`.
    /// \endrst
    static Result<std::vector<PhysicalDevice>> enumerate(Instance const&);

    /// \rst
    /// :vkspec:`chap6.html#VkPhysicalDeviceType`
    ///
    /// Supports formatting via :cpp:struct:`vx::detail::EnumFormatter`.
    /// \endrst
    enum class Type : uint32_t {
      OTHER          = 0,
      INTEGRATED_GPU = 1,
      DISCRETE_GPU   = 2,
      VIRTUAL_GPU    = 3,
      CPU            = 4,
    };

    /// \rst
    /// :vkspec:`chap6.html#VkPhysicalDeviceProperties`
    ///
    /// Note that this struct is quite large (around 600 bytes).
    /// \endrst
    struct Properties {
      Version apiVersion;
      Version driverVersion;
      uint32_t vendorID = 0;
      uint32_t deviceID = 0;
      Type deviceType   = Type::OTHER;
      std::string deviceName;
      UUID pipelineCacheUUID;

      VULKAN::VkPhysicalDeviceLimits limits                     = {};
      VULKAN::VkPhysicalDeviceSparseProperties sparseProperties = {};
    };

    /// \rst
    /// :vkspec:`chap6.html#vkGetPhysicalDeviceProperties`
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::PhysicalDevice::isValid()`.
    /// \endrst
    Properties getProperties() const;

    /// \rst
    /// :vkspec:`chap6.html#VkQueueFamilyProperties`
    /// \endrst
    struct QueueFamilyProperties {
      /// \rst
      /// This is just the index of this object in the vector returned by
      /// :cpp:func:`vx::PhysicalDevice::getQueueFamilyProperties()`.
      ///
      /// vx add-on.
      /// \endrst
      uint32_t index = 0;

      Bitset<QueueFlagBits> queueFlags;
      uint32_t queueCount         = 0;
      uint32_t timestampValidBits = 0;
      Extent3D minImageTransferGranularity;
    };

    /// \rst
    /// :vkspec:`chap6.html#vkGetPhysicalDeviceQueueFamilyProperties`
    ///
    /// The returned vector has at least one element.
    ///
    /// If given a valid :cpp:struct:`vx::Surface`, this is used to determine
    /// the :cpp:any:`vx::QueueFlagBits::SURFACE_SUPPORT` bit via
    /// :vkspec:`chap33.html#vkGetPhysicalDeviceSurfaceSupportKHR`.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::PhysicalDevice::isValid()`.
    /// \endrst
    Result<std::vector<QueueFamilyProperties>>
    getQueueFamilyProperties(Surface const& = {}) const;

    /// \rst
    /// :vkspec:`chap39.html#vkEnumerateDeviceExtensionProperties`
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::PhysicalDevice::isValid()`.
    /// \endrst
    Result<std::vector<ExtensionProperties>>
    enumerateExtensionProperties(std::string const& layerName = "") const;
  };

  /// \rst
  /// Combines a :cpp:struct:`vx::PhysicalDevice` with its
  /// :cpp:struct:`~vx::PhysicalDevice::Properties`,
  /// :cpp:struct:`~vx::PhysicalDevice::QueueFamilyProperties`, and an index to
  /// present a coherent data type that can be used to select between a number
  /// of physical devices.
  ///
  /// vx add-on.
  /// \endrst
  struct PhysicalDeviceInfo {
    size_t index = 0;
    PhysicalDevice physicalDevice;
    PhysicalDevice::Properties properties;
    std::vector<PhysicalDevice::QueueFamilyProperties> queueFamilyProperties;
    std::vector<ExtensionProperties> extensionProperties;

    /// \rst
    /// Defines a strong ordering on :cpp:struct:`vx::PhysicalDeviceInfo`\ 's
    /// with the function evaluating to :cpp:expr:`true` if *lhs* is more
    /// desirable than *rhs*.
    ///
    /// The ordering is implemented by lexicographically
    /// comparing :cpp:member:`~vx::PhysicalDevice::Properties::deviceType`
    /// (according to the order shown in the table below, with smaller numbers
    /// being more desirable), then
    /// :cpp:member:`~vx::PhysicalDevice::Properties::apiVersion` (with higher
    /// versions being more desirable), and finally
    /// :cpp:member:`~vx::PhysicalDeviceInfo::index`.
    ///
    /// 1. :cpp:enumerator:`~vx::PhysicalDevice::Type::DISCRETE_GPU`
    ///
    /// 2. :cpp:enumerator:`~vx::PhysicalDevice::Type::INTEGRATED_GPU`
    ///
    /// 3. :cpp:enumerator:`~vx::PhysicalDevice::Type::VIRTUAL_GPU`
    ///
    /// 4. :cpp:enumerator:`~vx::PhysicalDevice::Type::CPU`
    ///
    /// 5. :cpp:enumerator:`~vx::PhysicalDevice::Type::OTHER`
    ///
    /// .. warning::
    ///
    ///   The following code example can be quite expensive since
    ///   :cpp:struct:`vx::PhysicalDevice::Properties` is a large struct.
    ///
    /// .. code:: cpp
    ///
    ///   std::vector<PhysicalDeviceInfo> physDevInfos;
    ///   std::ranges::sort(physDevInfos, PhysicalDeviceInfo::Desirability);
    /// \endrst
    static bool isPreferable(
        PhysicalDeviceInfo const& lhs, PhysicalDeviceInfo const& rhs) noexcept;

    /// \rst
    /// Convenience function that combines calls to the following functions.
    ///
    ///   * :cpp:func:`vx::PhysicalDevice::enumerate()`
    ///   * :cpp:func:`vx::PhysicalDevice::getProperties()`
    ///   * :cpp:func:`vx::PhysicalDevice::getQueueFamilyProperties()`
    ///   * :cpp:func:`vx::PhysicalDevice::enumerateExtensionProperties()`
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::Instance::isValid()`.
    /// \endrst
    static Result<std::vector<PhysicalDeviceInfo>>
    enumerate(Instance const&, Surface const& = {});

    /// \rst
    /// Convenience function that calls
    /// :cpp:func:`vx::PhysicalDeviceInfo::enumerate()` and then returns the
    /// best :cpp:struct:`~vx::PhysicalDeviceInfo` according to the given
    /// *comparator*.
    ///
    /// The *comparator* is treated as a binary less-than function on
    /// PhysicalDeviceInfos. The smallest PhysicalDeviceInfo is then
    /// returned/selected.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::Instance::isValid()`.
    /// \endrst
    template<typename Comparator>
    static Result<PhysicalDeviceInfo>
    select(Instance const&, Surface const&, Comparator comparator);

    /// \rst
    /// Uses :cpp:func:`vx::PhysicalDeviceInfo::isPreferable()` as the
    /// *comparator* in the previous overload.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::Instance::isValid()`.
    /// \endrst
    static Result<PhysicalDeviceInfo>
    select(Instance const&, Surface const& = {});
  };

  /// \rst
  /// From the :cpp:member:`vx::PhysicalDeviceInfo::queueFamilyProperties`,
  /// identifies all those queue families that have at least the given
  /// *queueFlags* set. If there are multiple queue families that satisfy this
  /// condition, selects the one with (in this order):
  ///
  ///  * The highest number of
  ///    :cpp:member:`~vx::PhysicalDevice::QueueFamilyProperties::queueFlags`
  ///    set.
  ///  * The highest
  ///    :cpp:member:`~vx::PhysicalDevice::QueueFamilyProperties::queueCount`.
  ///  * The lowest
  ///    :cpp:member:`~vx::PhysicalDevice::QueueFamilyProperties::index`.
  ///
  /// Returns the
  /// :cpp:member:`~vx::PhysicalDevice::QueueFamilyProperties::index` of the
  /// corresponding queue family.
  ///
  /// vx add-on.
  /// \endrst
  Result<uint32_t> selectSingleQueue(
      std::vector<PhysicalDevice::QueueFamilyProperties> const&,
      Bitset<QueueFlagBits> queueFlags);
}

#include "PhysicalDevice.ipp"

#endif
