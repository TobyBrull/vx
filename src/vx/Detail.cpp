#include "Detail.hpp"

using namespace vx;
using namespace vx::detail;

std::vector<char const*>
vx::detail::charPtrVector(std::vector<std::string> const& v)
{
  std::vector<char const*> retval;
  retval.reserve(v.size());
  for (auto const& s: v) {
    retval.push_back(s.c_str());
  }
  return retval;
};
