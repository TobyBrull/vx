#ifndef __INCLUDE_GUARD_VX_GLFW_MONITOR_HPP
#define __INCLUDE_GUARD_VX_GLFW_MONITOR_HPP

#include "Glfw.hpp"

#include <memory>

namespace vx {
  /// \rst
  /// :glfw:`structGLFWvidmode.html`
  /// \endrst
  struct GlfwVideoMode {
    uint32_t width       = 0;
    uint32_t height      = 0;
    uint32_t redBits     = 0;
    uint32_t greenBits   = 0;
    uint32_t blueBits    = 0;
    uint32_t refreshRate = 0;

    bool operator==(GlfwVideoMode const&) const noexcept = default;
    std::partial_ordering operator<=>(GlfwVideoMode const&) const noexcept;
  };

  /// \rst
  /// Represents a physical monitor.
  ///
  /// GlfwMonitors are copyable.
  ///
  /// This object may only be used from the main thread.
  /// \endrst
  struct GlfwMonitor {
    GLFW::GLFWmonitor* handle = nullptr;

    /// \rst
    /// Whether the :cpp:member:`~vx::GlfwMonitor::handle` is :code:`nullptr`.
    /// \endrst
    bool isValid() const noexcept;

    /// \rst
    /// If no monitor can be found, the result will be an invalid
    /// :cpp:struct:`~vx::GlfwMonitor`, (i.e.,
    /// :cpp:func:`vx::GlfwMonitor::isValid()` will return :code:`false`).
    ///
    /// :glfw:`group__monitor.html` :code:`glfwGetPrimaryMonitor`
    /// \endrst
    static Result<GlfwMonitor> getPrimary() noexcept;

    /// \rst
    /// If no monitor can be found, the result will be an empty vector.
    ///
    /// :glfw:`group__monitor.html` :code:`glfwGetMonitors`
    /// \endrst
    static Result<std::vector<GlfwMonitor>> enumerate() noexcept;

    struct Info {
      /// \rst
      /// Position, in screen coordinates, of the upper-left corner.
      ///
      /// :glfw:`group__monitor.html` :code:`glfwGetMonitorPos`
      /// \endrst
      Offset2D pos;

      /// \rst
      /// Position, in screen coordinates, of the upper-left corner of the work
      /// area along with the work area size. The work area is defined as the
      /// area of the monitor not occluded by the operating system task bar
      /// where present.
      ///
      /// :glfw:`group__monitor.html` :code:`glfwGetMonitorWorkarea`
      /// \endrst
      Rect2D workarea;

      /// \rst
      /// Size, in millimetres, of the physical display area.
      ///
      /// :glfw:`group__monitor.html` :code:`glfwGetMonitorPhysicalSize`
      /// \endrst
      Extent2D physicalSizeMM;

      float contentScaleX = 0.0;
      /// \rst
      /// :glfw:`group__monitor.html` :code:`glfwGetMonitorContentScale`
      /// \endrst
      float contentScaleY = 0.0;

      /// \rst
      /// :glfw:`group__monitor.html` :code:`glfwGetMonitorName`
      /// \endrst
      std::string name;

      /// \rst
      /// :glfw:`group__monitor.html` :code:`glfwGetVideoModes`
      /// \endrst
      std::vector<GlfwVideoMode> availableVideoModes;

      /// \rst
      /// :glfw:`group__monitor.html` :code:`glfwGetVideoMode`
      /// \endrst
      GlfwVideoMode currentVideoMode;
    };

    /// \rst
    /// Retrieve various information about a monitor.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::GlfwMonitor::isValid()`.
    /// \endrst
    Result<Info> getInfo() const;
  };
}

#endif
