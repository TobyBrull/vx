#ifndef __INCLUDE_GUARD_VX_COMMAND_BUFFER_HPP
#define __INCLUDE_GUARD_VX_COMMAND_BUFFER_HPP

#include "Device.hpp"

namespace vx {
  /// \rst
  /// :vkspec:`chap6.html#commandbuffers-pools`
  /// \endrst
  struct CommandPool {
    /// The Vulkan handle.
    VULKAN::VkCommandPool handle = VK_NULL_HANDLE;

    /// \rst
    /// Corresponds to the :cpp:struct:`~vx::Device` for which this command pool
    /// was created.
    ///
    /// If the command pool handle is not :code:`VK_NULL_HANDLE`, this member
    /// must also not be :code:`VK_NULL_HANDLE` and refer to the
    /// :cpp:struct:`~vx::Device` from which this command pool was allocated.
    /// \endrst
    VULKAN::VkDevice device = VK_NULL_HANDLE;

    /// \rst
    /// :vkspec:`chap6.html#VkCommandPoolCreateInfo`
    /// \endrst
    struct CreateInfo {
      /// \rst
      /// :vkspec:`chap6.html#VkCommandPoolCreateFlagBits`
      /// \endrst
      enum class FlagBits : uint32_t {
        TRANSIENT = 0x00000001,
        BUFFER    = 0x00000002,
        // Provided by VK_VERSION_1_1
        PROTECTED = 0x00000004,
      };

      Bitset<FlagBits> flags;

      /// \rst
      /// Selects a queue family by its
      /// :cpp:member:`~vx::PhysicalDevice::QueueFamilyProperties::index`.
      /// \endrst
      uint32_t queueFamilyIndex = 0;

      /// Defaulted three-way comparison.
      constexpr auto operator<=>(CreateInfo const&) const noexcept = default;
    };

    /// \rst
    /// The :cpp:struct:`~vx::CommandPool::CreateInfo` used to
    /// :cpp:func:`~vx::CommandPool::create()` this
    /// :cpp:struct:`~vx::CommandPool`.
    /// \endrst
    CreateInfo createInfo;

    CommandPool() = default;

    CommandPool(CommandPool&&);
    CommandPool& operator=(CommandPool&&);

    ~CommandPool();

    /// \rst
    /// Whether :cpp:member:`~vx::CommandPool::handle` and
    /// :cpp:member:`~vx::CommandPool::device` are both not equal to the
    /// :code:`VK_NULL_HANDLE`.
    /// \endrst
    bool isValid() const noexcept;

    /// \rst
    /// :vkspec:`chap6.html#vkDestroyCommandPool`
    /// \endrst
    void destroy() noexcept;

    /// \rst
    /// :vkspec:`chap6.html#vkCreateCommandPool`
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If the given :cpp:struct:`~vx::Device` is not valid.
    /// \endrst
    static Result<CommandPool> create(Device const&, CreateInfo);
  };

  struct CommandBuffer {
    /// The Vulkan handle.
    VULKAN::VkCommandBuffer handle = VK_NULL_HANDLE;

    /// \rst
    /// The :cpp:struct:`~vx::Device` from which the command pool was
    /// allocated.
    ///
    /// If the command buffer handle is not :code:`VK_NULL_HANDLE`, this member
    /// must also not be :code:`VK_NULL_HANDLE` and refer to the
    /// :cpp:struct:`~vx::Device` from which this command buffer was
    /// allocated.
    /// \endrst
    VULKAN::VkDevice device = VK_NULL_HANDLE;

    /// \rst
    /// The :cpp:struct:`~vx::CommandPool` from which this command buffer was
    /// allocated.
    ///
    /// If the command buffer handle is not :code:`VK_NULL_HANDLE`, this member
    /// must also not be :code:`VK_NULL_HANDLE` and refer to the
    /// :cpp:struct:`~vx::CommandPool` from which this command buffer was
    /// allocated.
    /// \endrst
    VULKAN::VkCommandPool commandPool = VK_NULL_HANDLE;

    /// \rst
    /// :vkspec:`chap6.html#VkCommandBufferLevel`
    /// \endrst
    enum class Level {
      PRIMARY   = 0,
      SECONDARY = 1,
    };

    /// \rst
    /// :vkspec:`chap6.html#VkCommandBufferAllocateInfo`
    /// \endrst
    struct AllocateInfo {
      Level level                 = Level::PRIMARY;
      uint32_t commandBufferCount = 0;

      /// Defaulted three-way comparison.
      constexpr auto operator<=>(AllocateInfo const&) const noexcept = default;
    };

    /// \rst
    /// The :cpp:struct:`~vx::CommandBuffer::AllocateInfo` used to
    /// :cpp:func:`~vx::CommandBuffer::allocate()` this
    /// :cpp:struct:`~vx::CommandBuffer`.
    /// \endrst
    AllocateInfo allocateInfo;

    CommandBuffer() = default;

    CommandBuffer(CommandBuffer&&);
    CommandBuffer& operator=(CommandBuffer&&);

    ~CommandBuffer();

    /// \rst
    /// Whether :cpp:member:`~vx::CommandBuffer::handle`,
    /// :cpp:member:`~vx::CommandBuffer::device`, and
    /// :cpp:member:`~vx::CommandBuffer::commandPool` are all not equal to the
    /// :code:`VK_NULL_HANDLE`.
    /// \endrst
    bool isValid() const noexcept;

    /// \rst
    /// :vkspec:`chap6.html#vkFreeCommandBuffers`
    /// \endrst
    void free() noexcept;

    /// \rst
    /// :vkspec:`chap6.html#vkAllocateCommandBuffers`
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If the given :cpp:struct:`~vx::CommandPool` is not valid. If the given
    ///   :cpp:member:`~vx::CommandBuffer::AllocateInfo::commandBufferCount` is
    ///   zero.
    /// \endrst
    static Result<CommandBuffer> allocate(CommandPool const&, AllocateInfo);
  };
}

#include "CommandBuffer.ipp"

#endif
