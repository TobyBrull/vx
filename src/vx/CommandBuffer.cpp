#include "CommandBuffer.hpp"

using namespace vx;

VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(CommandPool::CreateInfo::FlagBits) = {
    {CommandPool::CreateInfo::FlagBits::TRANSIENT, "TRANSIENT"},
    {CommandPool::CreateInfo::FlagBits::BUFFER, "BUFFER"},
    {CommandPool::CreateInfo::FlagBits::PROTECTED, "PROTECTED"},
};

CommandPool::CommandPool(CommandPool&& other)
  : handle(std::move(other.handle))
  , device(std::move(other.device))
  , createInfo(std::move(other.createInfo))
{
  other.handle     = VK_NULL_HANDLE;
  other.device     = VK_NULL_HANDLE;
  other.createInfo = {};
}
CommandPool&
CommandPool::operator=(CommandPool&& other)
{
  handle     = std::move(other.handle);
  device     = std::move(other.device);
  createInfo = std::move(other.createInfo);

  other.handle     = VK_NULL_HANDLE;
  other.device     = VK_NULL_HANDLE;
  other.createInfo = {};

  return (*this);
}

CommandPool::~CommandPool()
{
  destroy();
}

bool
CommandPool::isValid() const noexcept
{
  return (handle != VK_NULL_HANDLE) && (device != VK_NULL_HANDLE);
}

void
CommandPool::destroy() noexcept
{
  if (isValid()) {
    vkDestroyCommandPool(device, handle, NULL);
  }
  handle     = VK_NULL_HANDLE;
  device     = VK_NULL_HANDLE;
  createInfo = {};
}

Result<CommandPool>
CommandPool::create(Device const& device, CreateInfo createInfo)
{
  VX_REQUIRE(device.isValid());

  VULKAN::VkCommandPoolCreateInfo const vkCreateInfo{
      .sType            = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      .pNext            = NULL,
      .flags            = createInfo.flags.data,
      .queueFamilyIndex = createInfo.queueFamilyIndex,
  };

  CommandPool retval;
  auto const vkResult = VULKAN::vkCreateCommandPool(
      device.handle, &vkCreateInfo, NULL, &retval.handle);
  if (vkResult != VK_SUCCESS) {
    VX_ASSERT(retval.handle == VK_NULL_HANDLE);
    return make_error_code(detail::fromVulkan(vkResult));
  }
  retval.device     = device.handle;
  retval.createInfo = std::move(createInfo);
  VX_ASSERT(retval.isValid());
  return {std::move(retval)};
}

VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(CommandBuffer::Level) = {
    {CommandBuffer::Level::PRIMARY, "PRIMARY"},
    {CommandBuffer::Level::SECONDARY, "SECONDARY"},
};

CommandBuffer::CommandBuffer(CommandBuffer&& other)
  : handle(std::move(other.handle))
  , device(std::move(other.device))
  , commandPool(std::move(other.commandPool))
  , allocateInfo(std::move(other.allocateInfo))
{
  other.handle       = VK_NULL_HANDLE;
  other.device       = VK_NULL_HANDLE;
  other.commandPool  = VK_NULL_HANDLE;
  other.allocateInfo = {};
}

CommandBuffer&
CommandBuffer::operator=(CommandBuffer&& other)
{
  handle       = std::move(other.handle);
  device       = std::move(other.device);
  commandPool  = std::move(other.commandPool);
  allocateInfo = std::move(other.allocateInfo);

  other.handle       = VK_NULL_HANDLE;
  other.device       = VK_NULL_HANDLE;
  other.commandPool  = VK_NULL_HANDLE;
  other.allocateInfo = {};

  return (*this);
}

CommandBuffer::~CommandBuffer()
{
  free();
}

bool
CommandBuffer::isValid() const noexcept
{
  return (handle != VK_NULL_HANDLE) && (device != VK_NULL_HANDLE) &&
      (commandPool != VK_NULL_HANDLE);
}

void
CommandBuffer::free() noexcept
{
  if (isValid()) {
    VULKAN::vkFreeCommandBuffers(
        device, commandPool, allocateInfo.commandBufferCount, &handle);
  }
  handle       = VK_NULL_HANDLE;
  device       = VK_NULL_HANDLE;
  commandPool  = VK_NULL_HANDLE;
  allocateInfo = {};
}

Result<CommandBuffer>
CommandBuffer::allocate(CommandPool const& commandPool, AllocateInfo allocInfo)
{
  VX_REQUIRE(commandPool.isValid());
  VX_REQUIRE(allocInfo.commandBufferCount >= 1);

  VULKAN::VkCommandBufferAllocateInfo const vkAllocInfo{
      .sType              = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .pNext              = NULL,
      .commandPool        = commandPool.handle,
      .level              = detail::toVulkan(allocInfo.level),
      .commandBufferCount = allocInfo.commandBufferCount,
  };

  CommandBuffer retval;
  auto const vkResult = VULKAN::vkAllocateCommandBuffers(
      commandPool.device, &vkAllocInfo, &retval.handle);
  if (vkResult != VK_SUCCESS) {
    VX_ASSERT(retval.handle == VK_NULL_HANDLE);
    return make_error_code(detail::fromVulkan(vkResult));
  }
  retval.device       = commandPool.device;
  retval.commandPool  = commandPool.handle;
  retval.allocateInfo = std::move(allocInfo);
  VX_ASSERT(retval.isValid());
  return {std::move(retval)};
}
