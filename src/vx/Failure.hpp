#ifndef __INCLUDE_GUARD_VX_FAILURE_HPP
#define __INCLUDE_GUARD_VX_FAILURE_HPP

#include <fmt/format.h>

// TODO:
// * #include <source_location>
//   - once done, the VX_... macros can become functions
// * Thread safety

namespace vx {
  /// \rst
  /// Executes the failure handler that is currently installed.
  ///
  /// The failure handler is invoked for certain types of user errors.
  ///
  /// Although, vx is compiled without exceptions, the failure handler may be
  /// replaced with a function that throws an exception.
  ///
  /// The default failure handler, which is installed at program startup, is
  /// :code:`std::exit(1)`.
  /// \endrst
  [[noreturn]] void runFailureHandler();

  /// \rst
  /// The type of a failure handler. This type is used by
  /// :cpp:func:`vx::FailureHandlerGuard::FailureHandlerGuard()`.
  /// \endrst
  using FailureHandlerPtr = decltype(&runFailureHandler);

  /// \rst
  /// RAII class for controlling the installed failure handler.
  ///
  /// FailureHandlerGuards are neither moveable nor copyable.
  ///
  /// Intended usage:
  ///
  /// .. code:: cpp
  ///
  ///   {
  ///     vx::FailureHandlerGuard const _{&myFailureHandler};
  ///
  ///     // ...
  ///   }
  /// \endrst
  struct FailureHandlerGuard {
    /// \rst
    /// The failure handler that will be installed in the destructor.
    /// \endrst
    FailureHandlerPtr const oldFailureHandler = nullptr;

    /// \rst
    /// Stores the old failure handler as
    /// :cpp:member:`~FailureHandlerGuard::oldFailureHandler` and then installs
    /// the given *newFailureHandler* as the failure handler.
    ///
    /// :Parameter *newFailureHandler*:
    ///   Is required to point to a function that never returns (cf. the
    ///   :code:`[[noreturn]]` attribute).
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If *newFailureHandler* is the :code:`nullptr`. This will, of course,
    ///   execute the old failure handler.
    /// \endrst
    [[nodiscard]] FailureHandlerGuard(FailureHandlerPtr newFailureHandler);

    /// \rst
    /// Installs the failure handler stored in
    /// :cpp:member:`~FailureHandlerGuard::oldFailureHandler`).
    /// \endrst
    ~FailureHandlerGuard();

    FailureHandlerGuard(FailureHandlerGuard&&) = delete;
    FailureHandlerGuard& operator=(FailureHandlerGuard&&) = delete;

    FailureHandlerGuard(FailureHandlerGuard const&) = delete;
    FailureHandlerGuard& operator=(FailureHandlerGuard const&) = delete;
  };

/// \rst
/// Macro used by vx to check preconditions in functions, i.e., user errors.
///
/// If the *condition* is not satisfied, the :cpp:func:`failure handler
/// <vx::runFailureHandler>` is invoked.
///
/// The ellipsis arguments are optional. If present, they must conform to the
/// signature of :cpp:func:`std::format()`. These arguments are used to write an
/// error message to :code:`stderr` before the failure handler is invoked. The
/// error message will also contain the file and line number.
///
/// Example:
///
/// .. code:: cpp
///
///   VX_REQUIRE(z == 0);
///   VX_REQUIRE_FMT(a == 42, "a (={}) should've been the answer", a);
/// \endrst
#define VX_REQUIRE_FMT(condition, formatString, ...)            \
  do {                                                          \
    if (!(condition)) {                                         \
      vx::detail::maybeFmtFailureHandler(                       \
          __FILE__,                                             \
          __LINE__,                                             \
          __func__,                                             \
          FMT_STRING(formatString) __VA_OPT__(, ) __VA_ARGS__); \
    }                                                           \
  } while (false)
#define VX_REQUIRE(condition)                                           \
  do {                                                                  \
    if (!(condition)) {                                                 \
      vx::detail::maybeFmtFailureHandler(__FILE__, __LINE__, __func__); \
    }                                                                   \
  } while (false)

/// \rst
/// Used to check for programming errors in vx, including postconditions, as
/// well as for documentation of the implementation of vx.
///
/// .. |ASSERTION| replace:: If the *condition* is not satisfied, the program is
///                          immediately stopped via :cpp:func:`std::abort()`.
/// \endrst
#define VX_ASSERT_FMT(condition, formatString, ...)             \
  do {                                                          \
    if (!(condition)) {                                         \
      vx::detail::maybeFmtAssertHandler(                        \
          __FILE__,                                             \
          __LINE__,                                             \
          __func__,                                             \
          FMT_STRING(formatString) __VA_OPT__(, ) __VA_ARGS__); \
    }                                                           \
  } while (false)
#define VX_ASSERT(condition)                                           \
  do {                                                                 \
    if (!(condition)) {                                                \
      vx::detail::maybeFmtAssertHandler(__FILE__, __LINE__, __func__); \
    }                                                                  \
  } while (false)

/// \rst
/// Used to check guarantees that are made by the Vulkan specification.
///
/// |ASSERTION|
/// \endrst
#define VX_VULKAN_SPEC_FMT(condition, formatString, ...) \
  VX_ASSERT_FMT(condition, formatString __VA_OPT__(, ) __VA_ARGS__)
#define VX_VULKAN_SPEC(condition) VX_ASSERT(condition)

/// \rst
/// Used to signify that a line of code can never be reached.
///
/// If the line of code is reach anyway, the program is immediately stopped via
/// :cpp:func:`std::abort()`.
/// \endrst
#define VX_UNREACHABLE() VX_ASSERT(false)

}

#include "Failure.ipp"

#endif
