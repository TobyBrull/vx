namespace vx::detail {
  /// \rst
  /// Helper template only used for documentation. The associated macros
  /// specialize a formatter for structs of relatively homogeneous members. The
  /// resulting formatter supports the same format specifications as the
  /// *UnderlyingType* given to :cpp:any:`VX_DETAIL_STRUCT_FORMATTER`.
  ///
  /// Intended usage:
  ///
  /// .. code::
  ///
  ///   // MyStruct.h:
  ///   struct MyStruct {
  ///     int field_a = 0;
  ///     int field_b = 0;
  ///   };
  ///
  ///   // MyStruct.ipp:
  ///   VX_DETAIL_STRUCT_FORMATTER(MyStruct, int)
  ///   VX_DETAIL_STRUCT_FORMATTER_MEMBER(field_a)
  ///   VX_DETAIL_STRUCT_FORMATTER_MEMBER(field_b)
  ///   VX_DETAIL_STRUCT_FORMATTER_END()
  ///
  ///   // User.cpp
  ///   MyStruct const s = {.field_a = 1, .field_b = 2 };
  ///   CHECK(fmt::format("{:3}", s) == "{field_a=  1, field_b=  2}");
  /// \endrst
  template<typename StructType>
  struct StructFormatter;

///
#define VX_DETAIL_STRUCT_FORMATTER(StructType, UnderlyingType)                \
  template<>                                                                  \
  struct fmt::formatter<StructType> : public fmt::formatter<UnderlyingType> { \
    template<typename FormatContext>                                          \
    auto format(StructType const& s, FormatContext& ctx)                      \
    {                                                                         \
      fmt::format_to(ctx.out(), "{{");                                        \
      bool first = true;                                                      \
      for_each(s, [&](auto const& val, auto const& name) {                    \
        if (!first) {                                                         \
          fmt::format_to(ctx.out(), ", ");                                    \
        }                                                                     \
        fmt::format_to(ctx.out(), "{}=", name);                               \
        fmt::formatter<UnderlyingType>::format(val, ctx);                     \
        first = false;                                                        \
      });                                                                     \
      return fmt::format_to(ctx.out(), "}}");                                 \
    }                                                                         \
                                                                              \
    template<typename Functor>                                                \
    void for_each(StructType const& s, Functor f) const                       \
    {
///
#define VX_DETAIL_STRUCT_FORMATTER_MEMBER(MemberName) \
  f(s.MemberName, #MemberName);

///
#define VX_DETAIL_STRUCT_FORMATTER_END() \
  }                                      \
  }                                      \
  ;

}
