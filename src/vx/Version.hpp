#ifndef __INCLUDE_GUARD_VX_VERSION_HPP
#define __INCLUDE_GUARD_VX_VERSION_HPP

#include "Failure.hpp"
#include "Vulkan.hpp"

namespace vx {
  /// \rst
  /// Represents a version of the form ``<major>.<minor>.<patch>``, e.g.,
  /// ``1.2.170``, in the 32-bit format used by
  /// :vkspec:`chap40.html#extendingvulkan-coreversions-versionnumbers`.
  ///
  /// Versions are copyable.
  /// \endrst
  struct Version {
    /// Data member.
    uint32_t data = 0;

    /// \rst
    /// Creates a version with the given *major*, *minor*, and *patch* version
    /// numbers.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If any of the three arguments doesn't fit into
    ///   :cpp:member:`~vx::Version::data`.
    /// \endrst
    static Version
    create(uint32_t major, uint32_t minor = 0, uint32_t patch = 0);

    uint32_t major() const noexcept; ///< The major version number.
    uint32_t minor() const noexcept; ///< The minor version number.
    uint32_t patch() const noexcept; ///< The patch version number.

    /// Versions are strongly ordered.
    constexpr auto operator<=>(Version const&) const noexcept = default;
  };
}

#include "Version.ipp"

#endif
