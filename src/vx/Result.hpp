#ifndef __INCLUDE_GUARD_VX_RESULT_HPP
#define __INCLUDE_GUARD_VX_RESULT_HPP

#include "Failure.hpp"
#include "Vulkan.hpp"

#include <fmt/format.h>

#include <optional>
#include <system_error>
#include <variant>

namespace vx {
  /// \rst
  /// Enum class representation of :vkspec:`chap4.html#fundamentals-returncodes`
  /// which is integrated with :code:`std::error_code`.
  ///
  /// Enumerators that start with :code:`ERROR_...` are considered errors and
  /// have negative values. The other enumerators with non-negative values are
  /// considered successes.
  ///
  /// Status codes with the :code:`..._KHR` or :code:`..._EXT` ending are from
  /// extensions to the Vulkan Specification.
  /// \endrst
  enum class StatusCode {
    SUCCESS                                            = 0,
    NOT_READY                                          = 1,
    TIMEOUT                                            = 2,
    EVENT_SET                                          = 3,
    EVENT_RESET                                        = 4,
    INCOMPLETE                                         = 5,
    ERROR_OUT_OF_HOST_MEMORY                           = -1,
    ERROR_OUT_OF_DEVICE_MEMORY                         = -2,
    ERROR_INITIALIZATION_FAILED                        = -3,
    ERROR_DEVICE_LOST                                  = -4,
    ERROR_MEMORY_MAP_FAILED                            = -5,
    ERROR_LAYER_NOT_PRESENT                            = -6,
    ERROR_EXTENSION_NOT_PRESENT                        = -7,
    ERROR_FEATURE_NOT_PRESENT                          = -8,
    ERROR_INCOMPATIBLE_DRIVER                          = -9,
    ERROR_TOO_MANY_OBJECTS                             = -10,
    ERROR_FORMAT_NOT_SUPPORTED                         = -11,
    ERROR_FRAGMENTED_POOL                              = -12,
    ERROR_UNKNOWN                                      = -13,
    ERROR_OUT_OF_POOL_MEMORY                           = -1000069000,
    ERROR_INVALID_EXTERNAL_HANDLE                      = -1000072003,
    ERROR_FRAGMENTATION                                = -1000161000,
    ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS               = -1000257000,
    ERROR_SURFACE_LOST_KHR                             = -1000000000,
    ERROR_NATIVE_WINDOW_IN_USE_KHR                     = -1000000001,
    SUBOPTIMAL_KHR                                     = 1000001003,
    ERROR_OUT_OF_DATE_KHR                              = -1000001004,
    ERROR_INCOMPATIBLE_DISPLAY_KHR                     = -1000003001,
    ERROR_VALIDATION_FAILED_EXT                        = -1000011001,
    ERROR_INVALID_SHADER_NV                            = -1000012000,
    ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT = -1000158000,
    ERROR_NOT_PERMITTED_EXT                            = -1000174001,
    ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT          = -1000255000,
    THREAD_IDLE_KHR                                    = 1000268000,
    THREAD_DONE_KHR                                    = 1000268001,
    OPERATION_DEFERRED_KHR                             = 1000268002,
    OPERATION_NOT_DEFERRED_KHR                         = 1000268003,
    PIPELINE_COMPILE_REQUIRED_EXT                      = 1000297000,
    // Additional error ocdes used by vx.
    ERROR_NO_PHYSICAL_DEVICE       = 2000000000,
    ERROR_NO_MATCHING_QUEUE_FAMILY = 2000000001,
  };

  /// \rst
  /// .. |VX_ERROR_CODE| replace:: ADL converter using a internal error category
  ///   to turn a :cpp:enum:`vx::StatusCode` into a
  ///
  /// |VX_ERROR_CODE| :code:`std::error_code`.
  /// \endrst
  std::error_code make_error_code(StatusCode) noexcept;

  /// \rst
  /// |VX_ERROR_CODE| :code:`std::error_condition`.
  /// \endrst
  std::error_condition make_error_condition(StatusCode) noexcept;

  /// \rst
  /// Effectively a :code:`std::tuple<std::error_code, std::string>`, but
  /// completely allocated on the heap to keep :code:`sizeof(ErrorMessage)`
  /// small. This, in turn, is done to keep :code:`sizeof`
  /// :cpp:struct:`vx::Result` small.
  ///
  /// Movable, but not copyable.
  ///
  /// The :cpp:member:`vx::ErrorMessage::data` must **never** be the
  /// :code:`nullptr`, except when in the moved-from state.
  /// \endrst
  struct ErrorMessage {
    std::unique_ptr<std::tuple<std::error_code, std::string>> data;

    ErrorMessage(std::error_code, std::string);

    std::strong_ordering operator<=>(ErrorMessage const& other) const noexcept;
    bool operator==(ErrorMessage const& other) const noexcept;
  };

  /// \rst
  /// Represents an error as present if :cpp:func:`vx::Result::isError()` is
  /// true. This type is returned by :cpp:func:`vx::Result::error()`.
  ///
  /// Movable, but not copyable.
  /// \endrst
  struct Error {
    std::variant<std::error_code, ErrorMessage> data;

    Error(std::error_code ec) noexcept;
    Error(ErrorMessage em) noexcept;

    /// \rst
    /// Returns the :code:`std::error_code` stored in the underlying
    /// representation.
    /// \endrst
    std::error_code code() const noexcept;

    auto operator<=>(Error const&) const noexcept = default;
  };

  /// \rst
  /// Only used to represent the "consumed" state in :cpp:struct:`vx::Result`.
  /// \endrst
  struct Consumed {
    auto operator<=>(Consumed const&) const noexcept = default;
  };

  /// \rst
  /// A type that either represents a successful return value of type :code:`T`
  /// or an error. Conceptually, a bit like a
  /// :cpp:expr:`std::variant<T,vx::Error>`, but with additional functionality
  /// to facilitate its use as a safe return type.
  ///
  /// More precisely, a Result holds the :cpp:member:`~vx::Result::payload`,
  /// which can represent the following states.
  ///
  /// +---------------------------------+----------+
  /// |                                 | state    |
  /// +=================================+==========+
  /// | :code:`T`                       | value    |
  /// +---------------------------------+----------+
  /// | :code:`std::error_code`         | error    |
  /// +---------------------------------+----------+
  /// | :cpp:struct:`vx::ErrorMessage`  | error    |
  /// +---------------------------------+----------+
  /// | :cpp:struct:`vx::Consumed`      | consumed |
  /// +---------------------------------+----------+
  ///
  /// If a Result is not in the "consumed" state when the destructor is run,
  /// :code:`std::abort()` will be called.
  ///
  /// Intended usage:
  ///
  /// .. code::
  ///
  ///   Result<float> sqrt(float const x)
  ///   {
  ///     if (x < 0.0) {
  ///       return make_error_code(std::errc::argument_out_of_domain);
  ///     }
  ///     return std::sqrt(x);
  ///   }
  ///
  ///   Result<int> my_function(float const x)
  ///   {
  ///     float const y = VX_TRY(sqrt(x));
  ///     return std::round(y);
  ///   }
  ///
  /// See also: :cpp:any:`vx::Result\< void \>`, :cpp:any:`VX_TRY <VX_TRY>`.
  /// \endrst
  template<typename T>
  struct Result {
    static_assert(!std::is_same_v<T, std::error_code>);
    static_assert(!std::is_same_v<T, ErrorMessage>);
    static_assert(!std::is_same_v<T, Error>);
    static_assert(!std::is_same_v<T, Consumed>);
    static_assert(!std::is_same_v<T, void>);

    using ValueType = T;

    /// The only data member.
    std::variant<T, std::error_code, ErrorMessage, Consumed> payload;

    /// \rst
    /// Calls :code:`std::abort()` if this result still contains an unconsumed
    /// error.
    /// \endrst
    ~Result();

    Result(T value) noexcept(std::is_nothrow_move_constructible_v<T>);
    Result(std::error_code) noexcept;
    Result(ErrorMessage) noexcept;
    Result(Error) noexcept;

    /// \rst
    /// Whether this Result is in the "value" state., i.e., whether the
    /// :cpp:member:`vx::Result::payload` contains a :code:`T`.
    /// \endrst
    bool isValue() const noexcept;

    /// \rst
    /// Whether this Result is in the "error" state, i.e., whether the
    /// :cpp:member:`vx::Result::payload` contains a :code:`std::error_code` or
    /// :cpp:struct:`vx::ErrorMessage`.
    /// \endrst
    bool isError() const noexcept;

    /// \rst
    /// Access the value. Puts this Result into the "consumed" state.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::Result::isValue()`.
    /// \endrst
    T value() &&;

    /// \rst
    /// Access the error. Puts this Result into the "consumed" state.
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::Result::isError()`.
    /// \endrst
    Error error() &&;

    /// \rst
    /// Put this Result into the "consumed" state.
    /// \endrst
    void consume() noexcept;

    constexpr auto operator<=>(Result<T> const&) const noexcept = default;
  };

  /// \rst
  /// Specialization of :cpp:struct:`vx::Result` for type :code:`void`.
  /// Essentially a :code:`std::optional<std::error_code>`.
  /// \endrst
  template<>
  struct Result<void> {
    using ValueType = void;

    std::variant<std::monostate, std::error_code, ErrorMessage, Consumed>
        payload;

    ~Result();

    Result() noexcept = default;
    Result(std::error_code) noexcept;
    Result(ErrorMessage) noexcept;
    Result(Error) noexcept;

    bool isValue() const noexcept;

    bool isError() const noexcept;

    void value() &&;

    Error error() &&;

    void consume() noexcept;

    constexpr auto operator<=>(Result<void> const&) const noexcept = default;
  };

/// \rst
/// Evaluates the *expression*, which is assumed to evaluate to a value of type
/// :cpp:struct:`vx::Result`. If in error, calls :code:`return` from the current
/// scope with that error value. Otherwise, returns the value contained in in
/// the :cpp:struct:`vx::Result`.
/// \endrst
#define VX_TRY(expression)                                            \
  ({                                                                  \
    auto result = (expression);                                       \
    static_assert(vx::detail::isResultType<decltype(result)>::value); \
    if (result.isError()) {                                           \
      return std::move(result).error();                               \
    }                                                                 \
    std::move(result).value();                                        \
  })

/// \rst
/// Similar to :cpp:any:`VX_TRY <VX_TRY>`, but ignores all errors (and also any
/// values). Instead of :cpp:expr:`VX_TRY_IGNORE(expression)` one could
/// equivalently just write :code:`(expression).consume()`. The use of
/// :code:`VX_TRY_IGNORE` should be preferred so that all calls to functions
/// that return a :cpp:struct:`vx::Result` can be found by grepping for
/// :code:`VX_TRY`.
/// \endrst
#define VX_TRY_IGNORE(expression)                                     \
  ({                                                                  \
    auto result = (expression);                                       \
    static_assert(vx::detail::isResultType<decltype(result)>::value); \
    result.consume();                                                 \
  })

/// \rst
/// Similar to :cpp:any:`VX_TRY <VX_TRY>`, but evaluates to *defaultExpression*
/// if *expression* returns in error.
/// \endrst
#define VX_TRY_OR(expression, defaultExpression)                            \
  ({                                                                        \
    auto result = (expression);                                             \
    static_assert(vx::detail::isResultType<decltype(result)>::value);       \
    auto retval =                                                           \
        result.isError() ? (defaultExpression) : std::move(result).value(); \
    result.consume();                                                       \
    retval;                                                                 \
  })
}

#include "Result.ipp"

#endif
