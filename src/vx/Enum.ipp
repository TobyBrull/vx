namespace vx::detail {
  /// \rst
  /// Helper template to instantiate a formatter for an enum. The
  /// resulting formatter supports the same format specifications as a string.
  ///
  /// Intended usage:
  ///
  /// .. code::
  ///
  ///   // MyEnum.h:
  ///   enum class MyEnum {
  ///     Value1 = 1,
  ///     Value2 = 2,
  ///   };
  ///
  ///   // MyEnum.ipp:
  ///   VX_DETAIL_ENUM_FORMATTER_DECLARE(MyEnum);
  ///
  ///   // MyEnum.cpp:
  ///   VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(MyEnum) = {
  ///       {MyEnum::Value1, "Value1"},
  ///       {MyEnum::Value2, "Value2"},
  ///   };
  /// \endrst
  template<typename EnumType>
  struct EnumFormatter : public fmt::formatter<std::string_view> {
    /// \rst
    /// Must be initialized to map each enum value to its formatted value.
    /// Use :cpp:any:`VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP`.
    /// \endrst
    static std::map<EnumType, std::string_view> const enumMap;

    template<typename FormatContext>
    auto format(EnumType const e, FormatContext& ctx)
    {
      auto const findIt = enumMap.find(e);
      VX_REQUIRE(findIt != enumMap.end());
      return fmt::formatter<std::string_view>::format(findIt->second, ctx);
    }
  };

/// \rst
/// Specialises the :cpp:class:`vx::detail::EnumFormatter` for EnumType.
/// \endrst
#define VX_DETAIL_ENUM_FORMATTER_DECLARE(EnumType)  \
  template<>                                        \
  std::map<EnumType, std::string_view> const        \
      vx::detail::EnumFormatter<EnumType>::enumMap; \
  template<>                                        \
  struct fmt::formatter<EnumType>                   \
    : public vx::detail::EnumFormatter<EnumType> {  \
  }

/// \rst
/// Slight abbreviation to initialize the map of a
/// :cpp:class:`vx::detail::EnumFormatter`.
/// \endrst
#define VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(EnumType) \
  template<>                                          \
  std::map<EnumType, std::string_view> const          \
      vx::detail::EnumFormatter<EnumType>::enumMap

}
