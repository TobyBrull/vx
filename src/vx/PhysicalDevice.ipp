namespace vx::detail {

  inline PhysicalDevice::Properties
  fromVulkan(VULKAN::VkPhysicalDeviceProperties const& orig) noexcept
  {
    PhysicalDevice::Properties retval{
        .apiVersion       = {.data = orig.apiVersion},
        .driverVersion    = {.data = orig.driverVersion},
        .vendorID         = orig.vendorID,
        .deviceID         = orig.deviceID,
        .deviceType       = static_cast<PhysicalDevice::Type>(orig.deviceType),
        .deviceName       = orig.deviceName,
        .limits           = orig.limits,
        .sparseProperties = orig.sparseProperties,
    };
    std::ranges::copy(
        orig.pipelineCacheUUID, retval.pipelineCacheUUID.data.begin());
    return retval;
  }

  inline PhysicalDevice::QueueFamilyProperties fromVulkan(
      uint32_t const index,
      VULKAN::VkQueueFamilyProperties const& orig) noexcept
  {
    return {
        .index              = index,
        .queueFlags         = Bitset<QueueFlagBits>{.data = orig.queueFlags},
        .queueCount         = orig.queueCount,
        .timestampValidBits = orig.timestampValidBits,
        .minImageTransferGranularity =
            fromVulkan(orig.minImageTransferGranularity),
    };
  }
}

namespace vx {
  template<typename Comparator>
  Result<PhysicalDeviceInfo> PhysicalDeviceInfo::select(
      Instance const& instance, Surface const& surface, Comparator comparator)
  {
    VX_REQUIRE(instance.isValid());

    auto const physDevInfos =
        VX_TRY(PhysicalDeviceInfo::enumerate(instance, surface));

    if (physDevInfos.empty()) {
      return make_error_code(StatusCode::ERROR_NO_PHYSICAL_DEVICE);
    }

    auto const physDevInfoIt =
        std::ranges::min_element(physDevInfos, comparator);
    VX_ASSERT(physDevInfoIt != physDevInfos.end());

    return *physDevInfoIt;
  }
}

VX_DETAIL_ENUM_FORMATTER_DECLARE(vx::QueueFlagBits);
VX_DETAIL_ENUM_FORMATTER_DECLARE(vx::PhysicalDevice::Type);
