#ifndef __INCLUDE_GUARD_VX_GLFW_HPP
#define __INCLUDE_GUARD_VX_GLFW_HPP

#include "Instance.hpp"
#include "PhysicalDevice.hpp"
#include "Result.hpp"

// "Result.hpp" includes the Vulkan header.
#include <GLFW/glfw3.h>

#include <memory>
#include <optional>

/// \rst
/// A macro that is defined to be sustituted for nothing. Only used to
/// explicitly mark names from the actual GLFW API.
/// \endrst
#define GLFW

namespace vx {

  /// \rst
  /// GLFW error codes.
  /// \endrst
  enum class GlfwErrorCode {
    NO_ERROR            = 0,
    NOT_INITIALIZED     = 0x00010001,
    NO_CURRENT_CONTEXT  = 0x00010002,
    INVALID_ENUM        = 0x00010003,
    INVALID_VALUE       = 0x00010004,
    OUT_OF_MEMORY       = 0x00010005,
    API_UNAVAILABLE     = 0x00010006,
    VERSION_UNAVAILABLE = 0x00010007,
    PLATFORM_ERROR      = 0x00010008,
    FORMAT_UNAVAILABLE  = 0x00010009,
    NO_WINDOW_CONTEXT   = 0x0001000A,
    // Additional error codes used by vx.
    ALREADY_INITIALIZED  = 0x10000001,
    VULKAN_NOT_SUPPORTED = 0x10000002,
  };

  /// \rst
  /// .. |VX_GLFW_ERROR_CODE| replace:: ADL converter using the internally
  ///   defined :code:``vx::detail::GlfwErrorCodeCategory`` to turn a
  ///   :cpp:enum:`vx::GlfwErrorCode` into a
  ///
  /// |VX_GLFW_ERROR_CODE| :code:`std::error_code`.
  /// \endrst
  std::error_code make_error_code(GlfwErrorCode) noexcept;

  /// \rst
  /// |VX_GLFW_ERROR_CODE| :code:`std::error_condition`.
  /// \endrst
  std::error_condition make_error_condition(GlfwErrorCode) noexcept;

  /// \rst
  /// RAII class for initializing and terminating GLFW.
  ///
  /// At the core of GLFW is an event queue. Certain events in the window system
  /// cause events to be posted onto this event queue; these events can also be
  /// triggered by the application itself. The following lists contains some
  /// examples of such events.
  ///
  ///   * When the user moves a :cpp:struct:`~vx::GlfwWindow` (or if the
  ///     application itself requests such a window to be moved).
  ///   * When a :cpp:struct:`~vx::GlfwWindow` is maximized.
  ///   * When a :cpp:struct:`~vx::GlfwWindow` is iconified.
  ///   * When the users presses a key in some :cpp:struct:`~vx::GlfwWindow`.
  ///
  /// To react to these events, event handlers (called *callbacks* in GLFW) can
  /// be registered with GLFW. The following list corresponds to the previous
  /// list and shows the GLFW functions that register the respective event
  /// handlers.
  ///
  ///   * :glfw:`group__window.html` :code:`glfwSetWindowPosCallback`
  ///   * :glfw:`group__window.html` :code:`glfwSetWindowMaximizeCallback`
  ///   * :glfw:`group__window.html` :code:`glfwSetWindowIconifyCallback`
  ///   * :glfw:`group__input.html` :code:`glfwSetKeyCallback`
  ///
  /// These event handlers can only be executed by the following three event
  /// processing functions.
  ///
  ///   * :cpp:func:`vx::Glfw::pollEvents()`
  ///   * :cpp:func:`vx::Glfw::waitEvents()`
  ///   * :cpp:func:`vx::Glfw::waitEventsTimeout()`
  ///
  /// These functions all pick the oldest event from the event
  /// queue and run the corresponding registered event handler (if any) until
  /// the event queue is empty; they only differ in their behaviour when run
  /// with an empty event queue.
  ///
  /// The application is responsible to regularly execute one of those three
  /// event processing functions. If the application does not do this, the
  /// window/application may be unresponsive and the window system may attempt
  /// to force-close the window/application.
  ///
  /// Glfws are moveable, but not copyable.
  ///
  /// This object and its functions (except where explicitly documented
  /// otherwise) may only be used from the main thread.
  /// \endrst
  struct Glfw {
    bool live = false;

    /// \rst
    /// :glfw:`group__init.html` :code:`glfwInit`
    /// \endrst
    [[nodiscard]] static Result<Glfw> init();

    Glfw() = default;

    /// \rst
    /// :glfw:`group__init.html` :code:`glfwTerminate`.
    /// \endrst
    ~Glfw();

    Glfw(Glfw&&);
    Glfw& operator=(Glfw&&);

    Glfw(Glfw const&) = delete;
    Glfw& operator=(Glfw const&) = delete;

    /// \rst
    /// :glfw:`group__window.html` :code:`glfwPollEvents`.
    ///
    /// This function processes only those events that are already in the event
    /// queue and then returns immediately.
    /// \endrst
    static Result<void> pollEvents() noexcept;

    /// \rst
    /// :glfw:`group__window.html` :code:`glfwWaitEvents`.
    /// This function puts the calling thread to sleep until at least one event
    /// is available in the event queue. Once one or more events are available,
    /// the events in the queue are processed and the function then returns
    /// immediately.
    /// \endrst
    static Result<void> waitEvents() noexcept;

    /// \rst
    /// :glfw:`group__window.html` :code:`glfwWaitEventsTimeout`.
    ///
    /// Like :cpp:func:`~vx::Glfw::waitEvents`, but sleeps for at most
    /// *timeoutSeconds*.
    /// \endrst
    static Result<void> waitEventsTimeout(double timeoutSeconds) noexcept;

    /// \rst
    /// Posts an empty event from the current thread to the event queue, causing
    /// :cpp:func:`~vx::Glfw::waitEvents` or
    /// :cpp:func:`~vx::Glfw::waitEventsTimeout` to return.
    ///
    /// Can be called from any thread.
    /// \endrst
    static Result<void> postEmptyEvent() noexcept;

    /// \rst
    /// Get list of extensions that need to be requested via
    /// :cpp:member:`vx::Instance::CreateInfo::enabledExtensionNames` in order
    /// to properly use Vulkan with GLFW.
    ///
    /// Guaranteed to include "VK_KHR_surface".
    ///
    /// Can be called from any thread.
    /// \endrst
    static Result<std::vector<std::string>>
    getRequiredInstanceExtensions() noexcept;

    /// \rst
    /// This function returns whether the specified queue family of the
    /// specified physical device supports presentation to the platform GLFW was
    /// built for.
    ///
    /// :glfw:`group__vulkan.html`
    /// :code:`glfwGetPhysicalDevicePresentationSupport`
    ///
    /// :cpp:func:`~vx::runFailureHandler()`:
    ///   If not :cpp:func:`vx::Instance::isValid()` or if not
    ///   :cpp:func:`vx::PhysicalDevice::isValid()`.
    /// \endrst
    static Result<bool> hasPresentationSupport(
        Instance const&, PhysicalDevice const&, uint32_t queueFamilyIndex);
  };
}

#include "Glfw.ipp"

#endif
