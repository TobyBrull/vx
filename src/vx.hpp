#ifndef __INCLUDE_GUARD_VX_HPP
#define __INCLUDE_GUARD_VX_HPP

// clang-format off
#include "vx/Instance.hpp"
#include "vx/PhysicalDevice.hpp"
#include "vx/Device.hpp"
#include "vx/CommandBuffer.hpp"

#include "vx/Glfw.hpp"
#include "vx/GlfwMonitor.hpp"
#include "vx/GlfwWindow.hpp"

#include "vx/Vulkan.hpp"
#include "vx/Failure.hpp"
#include "vx/Result.hpp"
#include "vx/Enum.hpp"
#include "vx/Struct.hpp"
#include "vx/Bitset.hpp"
#include "vx/Version.hpp"
// clang-format on

#endif
