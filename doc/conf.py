# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'vx'
copyright = '2021, Toby Brull'
author = 'Toby Brull'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.graphviz',
    'sphinx.ext.extlinks',
    'sphinx.ext.todo',
    'breathe',
]

todo_include_todos = True

extlinks = {
    'vkspec': (
        'https://www.khronos.org/registry/vulkan/specs/1.2-extensions/html/%s',
        'vkspec://',
    ),
    'glfw': (
        'https://www.glfw.org/docs/latest/%s',
        'glfw://',
    ),
}

breathe_default_project = "my_default_project"

# Add any paths that contain templates here, relative to this directory.
templates_path = []

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'

html_theme_options = {
    'description': 'C++ Vulkan Wrapper',
    'fixed_sidebar': True,
    #'page_width': "80%",
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = []

# -- Options for LaTeX output -------------------------------------------------

latex_documents = [('index', 'doc.tex', project, author, 'manual')]
