Result.hpp
==========

.. doxygenenum:: vx::StatusCode

.. doxygenfunction:: vx::make_error_code(StatusCode const c) noexcept

.. doxygenfunction:: vx::make_error_condition(StatusCode const c) noexcept

|

.. doxygenstruct:: vx::ErrorMessage
  :members:
  :undoc-members:

|

.. doxygenstruct:: vx::Error
  :members:
  :undoc-members:

.. doxygenstruct:: fmt::formatter< vx::Error >

|
|

.. doxygenstruct:: vx::Result
  :members:
  :undoc-members:

|

.. doxygenstruct:: vx::Result< void >
  :members:
  :undoc-members:

|

.. doxygendefine:: VX_TRY
