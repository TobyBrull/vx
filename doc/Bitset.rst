Bitset.hpp
==========

.. doxygenfunction:: vx::areFlagBits

|

.. doxygenstruct:: vx::Bitset
  :members:

.. doxygenfunction:: vx::createBitset
