Version.hpp
===========

.. doxygenstruct:: vx::Version
  :members:

|

.. doxygenstruct:: fmt::formatter< vx::Version >
