Vulkan.hpp
==========

.. doxygendefine:: VULKAN

|

.. doxygenstruct:: vx::UUID
  :members:
  :undoc-members:

|

Common Object Types
-------------------

.. doxygenstruct:: vx::Offset2D
  :members:
  :undoc-members:

|

.. doxygenstruct:: vx::Offset3D
  :members:
  :undoc-members:

|

.. doxygenstruct:: vx::Extent2D
  :members:
  :undoc-members:

|

.. doxygenstruct:: vx::Extent3D
  :members:
  :undoc-members:

|

.. doxygenstruct:: vx::Rect2D
  :members:
  :undoc-members:
