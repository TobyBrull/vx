Welcome to vx's documentation!
==============================

vx is a C++20 wrapper around the core bits of the Vulkan Specification, cf.
:vkspec:`index.html`.

.. note::
  The previous sentence uses the ``vkspec://...`` syntax to refer to the Vulkan
  Specification. This form is used throughout the documentation of vx.

.. toctree::
  :maxdepth: 1
  :caption: Core API:

  Instance
  Surface
  PhysicalDevice
  Device
  CommandBuffer

.. toctree::
  :maxdepth: 1
  :caption: GLFW wrapper:

  Glfw
  GlfwMonitor
  GlfwWindow

.. toctree::
  :maxdepth: 1
  :caption: Texts:

  summary
  coding-guidelines

.. toctree::
  :maxdepth: 1
  :caption: Auxilliary:

  Vulkan
  Failure
  Result
  Enum
  Struct
  Detail
  Bitset
  Version

TODO list
=========

.. todolist::

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
