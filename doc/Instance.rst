Instance.hpp
============

.. doxygenstruct:: vx::ApplicationInfo
  :members:
  :undoc-members:

|

.. doxygenstruct:: vx::LayerProperties
  :members:
  :undoc-members:

|

.. doxygenstruct:: vx::Instance
  :members:
  :undoc-members:
