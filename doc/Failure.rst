.. _failure-header:

Failure.hpp
===========

.. doxygenfunction:: vx::runFailureHandler

.. doxygentypedef:: vx::FailureHandlerPtr

|

.. doxygenstruct:: vx::FailureHandlerGuard
  :members:

Macros
------

.. doxygendefine:: VX_REQUIRE
.. doxygendefine:: VX_REQUIRE_FMT

.. doxygendefine:: VX_ASSERT
.. doxygendefine:: VX_ASSERT_FMT

.. doxygendefine:: VX_VULKAN_SPEC
.. doxygendefine:: VX_VULKAN_SPEC_FMT

.. doxygendefine:: VX_UNREACHABLE
