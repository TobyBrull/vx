Struct.hpp
==========

.. doxygenstruct:: vx::detail::StructFormatter
  :members:

|
|

.. doxygendefine:: VX_DETAIL_STRUCT_FORMATTER
.. doxygendefine:: VX_DETAIL_STRUCT_FORMATTER_MEMBER
.. doxygendefine:: VX_DETAIL_STRUCT_FORMATTER_END
