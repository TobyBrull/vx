PhysicalDevice.hpp
==================

.. doxygenstruct:: vx::PhysicalDevice
  :members:
  :undoc-members:

|
|

.. doxygenenum:: vx::PhysicalDevice::Type

|
|

.. doxygenstruct:: vx::PhysicalDevice::Properties
  :members:
  :undoc-members:

|
|

.. doxygenenum:: vx::QueueFlagBits

.. doxygenstruct:: vx::PhysicalDevice::QueueFamilyProperties
  :members:
  :undoc-members:

|
|

vx Add-Ons
----------

.. doxygenstruct:: vx::PhysicalDeviceInfo
  :members:
  :undoc-members:
