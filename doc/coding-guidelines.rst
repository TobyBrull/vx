Coding Guidelines
=================

* Try to keep use of the standard C++ library to a minimum.

* vx doesn't use exceptions and is compiled with :code:`-fno-exceptions` by
  default. Error handling is done via a :cpp:struct:`vx::Result` or via the
  macros in :ref:`failure-header`.

  * Never use libfmt directly; only through the :code:`VX_...` macros defined
    in :ref:`failure-header`. This guarantees that all format strings are
    checked at compile time which, in turn, guarantees that libfmt won't throw
    an exception inside vx.

  * Check all user-provided parameters before acquiring any resources (like
    allocating memory) using the :cpp:any:`VX_REQUIRE` macros. This way, even
    if the user compiles his code without :code:`-fno-exceptions` and installs
    an exception throwing failure handler, no resource leaks occur.

  * Use :code:`noexcept` where possible. Do not use :code:`noexcept` if a
    user-installed failure handler could lead to an exception being thrown from
    the function.
