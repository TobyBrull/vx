High-level summary
==================

.. |Instance| replace:: :cpp:struct:`~vx::Instance`
.. |PhysicalDevice| replace:: :cpp:struct:`~vx::PhysicalDevice`
.. |Device| replace:: :cpp:struct:`~vx::Device`
.. |Queue| replace:: :cpp:struct:`~vx::Queue`
.. |QueueFamily| replace:: :cpp:struct:`QueueFamily <vx::PhysicalDevice::QueueFamilyProperties>`

Vulkan has no global state. An *application* has to create an |Instance| to communicate
with the `Vulkan Loader <https://github.com/KhronosGroup/Vulkan-Loader>`__,
which is the front-end to the *Vulkan implementation*.

Each |Instance| has one or more |PhysicalDevice|\ s. A |PhysicalDevice| usually
represents a GPU.

Each |PhysicalDevice| has one or more |Queue|\ s, which are grouped into
|QueueFamily|\ s. A logical |Device| can be created from a |PhysicalDevice| by
selecting one or more |QueueFamily|\ s.

Layers & Extensions
-------------------

When creating an |Instance| one can enable a set of *layers* (earlier version of
the Vulkan API also had device-level layers, but those are now deprecated). A
layer can hook into the Vulkan API functions to transparently add functionality
like error checking.

The Vulkan API also supports *extensions*. An extension adds new functions to
the Vulkan API. There are three different types of *extensions*:
instance-level, layer-level, and device-level. 
