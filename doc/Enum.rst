Enum.hpp
========

.. doxygenstruct:: vx::detail::EnumFormatter
  :members:

|
|

.. doxygendefine:: VX_DETAIL_ENUM_FORMATTER_DECLARE

|

.. doxygendefine:: VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP

|

.. doxygenfunction:: vx::enumValues
