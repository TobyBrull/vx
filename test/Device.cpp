#include "vx/Device.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;

TEST_CASE("QueueCreateInfo::FlagBits", "[Device][Bitset]")
{
  CHECK(areFlagBits<Device::QueueCreateInfo::FlagBits>());
}

TEST_CASE("Device", "[Device]")
{
  auto env = test::environment(test::Environment::PURE_VULKAN);

  {
    Device::CreateInfo const createInfo{
        .queueCreateInfos = {{
            .queueFamilyIndex = 0,
            .queuePriorities  = std::vector{0.77f},
        }},
    };

    auto dev = VX_TRY_REQUIRE(
        Device::create(env.physDevInfo.physicalDevice, createInfo));

    CHECK(dev.createInfo == createInfo);
    {
      auto const& qCreateInfos = dev.createInfo.queueCreateInfos;
      CHECK(qCreateInfos.size() == 1);
      CHECK(qCreateInfos.at(0).queueCount() == 1);
    }
  }

  {
    uint32_t const qFamIndex = VX_TRY_REQUIRE(selectSingleQueue(
        env.physDevInfo.queueFamilyProperties,
        createBitset(QueueFlagBits::GRAPHICS)));

    auto dev = VX_TRY_REQUIRE(Device::createWithSingleQueue(
        env.physDevInfo.physicalDevice, qFamIndex));

    {
      auto const& qCreateInfos = dev.createInfo.queueCreateInfos;
      CHECK(qCreateInfos.size() == 1);
      CHECK(qCreateInfos.at(0).queueCount() == 1);
    }
  }
}
