#include "vx/Struct.hpp"

#include <catch2/catch.hpp>

using namespace vx;

// MyStruct.h:
struct MyStruct {
  int field_a = 0;
  int field_b = 0;
};

// MyStruct.ipp:
VX_DETAIL_STRUCT_FORMATTER(MyStruct, int)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(field_a)
VX_DETAIL_STRUCT_FORMATTER_MEMBER(field_b)
VX_DETAIL_STRUCT_FORMATTER_END()

TEST_CASE("StructFormatter", "[Struct][StructFormatter]")
{
  MyStruct const s = {.field_a = 1, .field_b = 2};
  CHECK(fmt::format("{:3}", s) == "{field_a=  1, field_b=  2}");
}
