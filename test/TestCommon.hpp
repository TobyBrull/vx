#ifndef __INCLUDE_GUARD_TEST_TEST_COMMON_HPP
#define __INCLUDE_GUARD_TEST_TEST_COMMON_HPP

#include "vx/CommandBuffer.hpp"
#include "vx/Device.hpp"
#include "vx/Failure.hpp"
#include "vx/GlfwWindow.hpp"
#include "vx/PhysicalDevice.hpp"

namespace vx::test {
  // Exception class as thrown by :cpp:func:`vx::test::throwFailureException`.
  class FailureException : public std::exception {
   public:
    virtual char const* what() const noexcept override final
    {
      return "FailureException";
    }
  };

  // Function to be used as a failure handler that simply throws a
  // :cpp:class:`vx::test::FailureException`.
  //
  // auto const _ = FailureHandlerGuard::create(&test::throwFailureException);
  //
  [[noreturn]] void throwFailureException();

#define VX_TRY_REQUIRE(expression)                                      \
  ({                                                                    \
    auto result = (expression);                                         \
    if (result.isError()) {                                             \
      INFO(fmt::format(                                                 \
          "VX_TRY_REQUIRE failed with {}", std::move(result).error())); \
      REQUIRE(false);                                                   \
    }                                                                   \
    std::move(result).value();                                          \
  })

  struct Environment {
    enum Type {
      PURE_VULKAN,
      WITH_GLFW,
      WITH_SURFACE,
    };

    Glfw glfw;
    Instance instance;
    GlfwWindow window;
    Surface surface;
    PhysicalDeviceInfo physDevInfo;
    uint32_t queueFamilyIndex = 0;
    Device device;
    CommandPool commandPool;
    CommandBuffer commandBuffer;
  };

  Environment environment(Environment::Type);
}

#endif
