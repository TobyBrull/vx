#include "vx/Version.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;

TEST_CASE("Version", "[Version]")
{
  CHECK(Version().data == 0);
  CHECK(Version::create(1).data == VK_API_VERSION_1_0);
  CHECK(Version::create(1, 2, 3) < Version::create(2, 2, 3));
  CHECK(Version::create(1, 2, 3) < Version::create(2, 1, 1));
  CHECK(Version::create(1, 2, 3) < Version::create(1, 3, 1));
  CHECK(Version::create(2, 3, 4) < Version::create(2, 3, 5));
  CHECK(Version::create(1, 2, 3) == Version::create(1, 2, 3));
  {
    const auto v = Version::create(4, 2, 1);
    CHECK(v.major() == 4);
    CHECK(v.minor() == 2);
    CHECK(v.patch() == 1);
  }

  {
    auto const v = Version::create(1, 2, 136);
    CHECK(fmt::format("v{:}-test", v) == "v1.2.136-test");
    CHECK(fmt::format("v{:v>{}}-test", v, 10) == "vvvv1.2.136-test");
  }

  {
    FailureHandlerGuard const _{&test::throwFailureException};

    CHECK_NOTHROW(Version::create(1 << 9, 1 << 9, 1 << 11));
    CHECK_THROWS_AS(Version::create(1 << 10, 1, 1), test::FailureException);
    CHECK_THROWS_AS(Version::create(1, 1 << 10, 1), test::FailureException);
    CHECK_THROWS_AS(Version::create(1, 1, 1 << 12), test::FailureException);
  }
}
