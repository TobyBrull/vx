#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

#include <cstdlib>

int
main(int argc, char* argv[])
{
  // Try to set up an environment for the test process that always loads the
  // validation layer automatically in such a way that it crashes the program on
  // every error.
  {
    setenv("VK_INSTANCE_LAYERS", "VK_LAYER_KHRONOS_validation", false);

    std::ofstream ofs("vk_layer_settings.txt", std::ios_base::out);
    ofs << "khronos_validation.debug_action = VK_DBG_LAYER_ACTION_BREAK\n";
  }

  return Catch::Session().run(argc, argv);
}
