#include "vx/Result.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;
using namespace vx::detail;

using namespace std::literals;

TEST_CASE("StatusCode", "[StatusCode]")
{
  auto const success = std::error_condition{};
  CHECK(make_error_code(StatusCode::SUCCESS) == success);
  CHECK(make_error_code(StatusCode::EVENT_SET) == success);
  CHECK(make_error_code(StatusCode::EVENT_RESET) == success);

  CHECK(make_error_code(StatusCode::NOT_READY) == success);
  CHECK(make_error_code(StatusCode::NOT_READY) == StatusCode::NOT_READY);

  CHECK(make_error_code(StatusCode::ERROR_UNKNOWN) != success);
  CHECK(
      make_error_code(StatusCode::ERROR_UNKNOWN) == StatusCode::ERROR_UNKNOWN);

  {
    auto const ec = make_error_condition(StatusCode::ERROR_FRAGMENTED_POOL);
    CHECK(ec.category().name() == "vx::StatusCode"s);
    CHECK(ec.value() == static_cast<int>(StatusCode::ERROR_FRAGMENTED_POOL));
  }

  {
    auto const ec = make_error_code(StatusCode::ERROR_FRAGMENTED_POOL);
    CHECK(ec.category().name() == "vx::StatusCode"s);
    CHECK(ec.value() == static_cast<int>(StatusCode::ERROR_FRAGMENTED_POOL));

    {
      auto const result = fmt::format("status-code = |{:^30}|", Error{ec});
      CHECK(result == "status-code = |    ERROR_FRAGMENTED_POOL     |");
    }
  }

  {
    auto const ec = make_error_code(StatusCode::ERROR_OUT_OF_HOST_MEMORY);
    CHECK(ec.message() == "ERROR_OUT_OF_HOST_MEMORY");
    CHECK(fmt::to_string(Error{ec}) == "ERROR_OUT_OF_HOST_MEMORY");
  }
}

TEST_CASE("StatusCode matches VkResult", "[StatusCode]")
{
#define VK_RESULT_CHECK(x)                    \
  CHECK(fromVulkan(VK_##x) == StatusCode::x); \
  CHECK(static_cast<int>(VK_##x) == static_cast<int>(StatusCode::x));

  VK_RESULT_CHECK(SUCCESS)
  VK_RESULT_CHECK(NOT_READY)
  VK_RESULT_CHECK(TIMEOUT)
  VK_RESULT_CHECK(EVENT_SET)
  VK_RESULT_CHECK(EVENT_RESET)
  VK_RESULT_CHECK(INCOMPLETE)
  VK_RESULT_CHECK(ERROR_OUT_OF_HOST_MEMORY)
  VK_RESULT_CHECK(ERROR_OUT_OF_DEVICE_MEMORY)
  VK_RESULT_CHECK(ERROR_INITIALIZATION_FAILED)
  VK_RESULT_CHECK(ERROR_DEVICE_LOST)
  VK_RESULT_CHECK(ERROR_MEMORY_MAP_FAILED)
  VK_RESULT_CHECK(ERROR_LAYER_NOT_PRESENT)
  VK_RESULT_CHECK(ERROR_EXTENSION_NOT_PRESENT)
  VK_RESULT_CHECK(ERROR_FEATURE_NOT_PRESENT)
  VK_RESULT_CHECK(ERROR_INCOMPATIBLE_DRIVER)
  VK_RESULT_CHECK(ERROR_TOO_MANY_OBJECTS)
  VK_RESULT_CHECK(ERROR_FORMAT_NOT_SUPPORTED)
  VK_RESULT_CHECK(ERROR_FRAGMENTED_POOL)
  VK_RESULT_CHECK(ERROR_UNKNOWN)
  VK_RESULT_CHECK(ERROR_OUT_OF_POOL_MEMORY)
  VK_RESULT_CHECK(ERROR_INVALID_EXTERNAL_HANDLE)
  VK_RESULT_CHECK(ERROR_FRAGMENTATION)
  VK_RESULT_CHECK(ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS)
  VK_RESULT_CHECK(ERROR_SURFACE_LOST_KHR)
  VK_RESULT_CHECK(ERROR_NATIVE_WINDOW_IN_USE_KHR)
  VK_RESULT_CHECK(SUBOPTIMAL_KHR)
  VK_RESULT_CHECK(ERROR_OUT_OF_DATE_KHR)
  VK_RESULT_CHECK(ERROR_INCOMPATIBLE_DISPLAY_KHR)
  VK_RESULT_CHECK(ERROR_VALIDATION_FAILED_EXT)
  VK_RESULT_CHECK(ERROR_INVALID_SHADER_NV)
  VK_RESULT_CHECK(ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT)
  VK_RESULT_CHECK(ERROR_NOT_PERMITTED_EXT)
  VK_RESULT_CHECK(ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT)
  VK_RESULT_CHECK(THREAD_IDLE_KHR)
  VK_RESULT_CHECK(THREAD_DONE_KHR)
  VK_RESULT_CHECK(OPERATION_DEFERRED_KHR)
  VK_RESULT_CHECK(OPERATION_NOT_DEFERRED_KHR)
  VK_RESULT_CHECK(PIPELINE_COMPILE_REQUIRED_EXT)

#undef VK_RESULT_CHECK
}

TEST_CASE("ErrorMessage", "[Result]")
{
  std::error_code const ec{};
  auto const ec1 = make_error_code(StatusCode::ERROR_FRAGMENTATION);
  ErrorMessage const em1(ec, "Message 1");
  ErrorMessage em2_a(ec1, "Message 2");
  auto const em2 = std::move(em2_a);
  ErrorMessage const em3(ec1, "Message 2");
  CHECK(em1 != em3);
  std::get<0>(*em3.data) = ec;
  std::get<1>(*em3.data) = "Message 1";
  CHECK(em1 == em3);
  std::get<1>(*em3.data) = "ZZZZZZZZZ";
  CHECK(em1 < em3);
}

TEST_CASE("Error", "[Result]")
{
  auto const ec1 = make_error_code(StatusCode::ERROR_FRAGMENTATION);
  Error const e1(ec1);
  Error const e2(ErrorMessage{ec1, "wrong file"});
  CHECK(e1 != e2);
  CHECK(e1 < e2);
  CHECK(e1.code() == e2.code());

  CHECK(fmt::to_string(e1) == "ERROR_FRAGMENTATION");
  CHECK(fmt::to_string(e2) == "wrong file");
}

TEST_CASE("Result", "[Result]")
{
  static_assert(sizeof(ErrorMessage) == sizeof(std::string const*));
  static_assert(
      sizeof(Result<char>) ==
      sizeof(std::variant<char, std::error_code, std::string const*>));
  static_assert(!std::is_error_code_enum<StatusCode>::value);
  static_assert(std::is_error_condition_enum<StatusCode>::value);

  FailureHandlerGuard const _{&test::throwFailureException};

  auto const ec = make_error_code(StatusCode::ERROR_FRAGMENTATION);

  {
    Result<int> r = make_error_code(StatusCode::ERROR_DEVICE_LOST);
    CHECK(r.isError());
    CHECK(std::move(r).error().code() == StatusCode::ERROR_DEVICE_LOST);
    CHECK_THROWS_AS(std::move(r).value(), test::FailureException);
  }

  {
    Result<int> r(42);
    Result<int> r2{ec};
    CHECK(r != r2);
    CHECK(r < r2);
    CHECK(!r.isError());
    CHECK(std::move(r).value() == 42);
    CHECK_THROWS_AS(std::move(r).error(), test::FailureException);
    r2.consume();
  }

  {
    Result<int> re(ec);
    Result<int> rem(ErrorMessage{ec, "Message 1"});
    Result<int> rhs1{Error{ec}};
    Result<int> rhs2{Error{ErrorMessage{ec, "Message 2"}}};
    Result<int> rhs3{Error{ErrorMessage{ec, "Message 1"}}};
    CHECK(re == rhs1);
    CHECK(rem != rhs2);
    CHECK(rem == rhs3);
    CHECK(rhs1 != rhs2);
    CHECK(rhs1 != rhs3);
    CHECK(rhs2 != rhs3);
    CHECK(fmt::to_string(std::move(rem).error()) == "Message 1");
    re.consume();
    rem.consume();
    rhs1.consume();
    rhs2.consume();
    rhs3.consume();
  }

  {
    Result<int> re(ec);
    Result<int> rem(ErrorMessage{ec, "Message 1"});
    CHECK(std::move(re).error().code() == std::move(rem).error().code());
    re.consume();
    rem.consume();
  }

  {
    Result<void> r = make_error_code(StatusCode::ERROR_DEVICE_LOST);
    CHECK(r.isError());
    CHECK(std::move(r).error().code() == StatusCode::ERROR_DEVICE_LOST);
    r.consume();
  }

  {
    Result<void> r{};
    Result<void> rhs{ec};
    CHECK(!r.isError());
    CHECK(r != rhs);
    CHECK(r < rhs);
    CHECK_THROWS_AS(std::move(r).error(), test::FailureException);
    r.consume();
    rhs.consume();
  }

  {
    Result<void> re(ec);
    Result<void> rem(ErrorMessage{ec, "Message 1"});
    Result<void> rhs1{Error{ec}};
    Result<void> rhs2{Error{ErrorMessage{ec, "Message 2"}}};
    Result<void> rhs3{Error{ErrorMessage{ec, "Message 1"}}};
    CHECK(re == rhs1);
    CHECK(rem != rhs2);
    CHECK(rem == rhs3);
    CHECK(fmt::to_string(std::move(rem).error()) == "Message 1");
    re.consume();
    rhs1.consume();
    rhs2.consume();
    rhs3.consume();
  }

  {
    Result<void> re(ec);
    Result<void> rem(ErrorMessage{ec, "Message 1"});
    CHECK(std::move(re).error().code() == std::move(rem).error().code());
    rem.consume();
  }

  auto result = []() -> Result<std::string> {
    auto const f = []() -> Result<int> {
      return make_error_code(StatusCode::ERROR_DEVICE_LOST);
    };

    int const i = VX_TRY(f());
    return fmt::to_string(i);
  }();
  REQUIRE(result.isError());
  CHECK(
      std::move(result).error().code() ==
      make_error_code(StatusCode::ERROR_DEVICE_LOST));
}

namespace {
  Result<float> sqrt(float const x)
  {
    if (x < 0.0) {
      return make_error_code(std::errc::argument_out_of_domain);
    }
    return std::sqrt(x);
  }

  Result<int> my_function(float const x)
  {
    float const y = VX_TRY(sqrt(x));
    return std::round(y);
  }
}

TEST_CASE("Result doc", "[Result]")
{
  CHECK(VX_TRY_REQUIRE(my_function(17)) == 4);
  CHECK(VX_TRY_REQUIRE(my_function(0)) == 0);
  VX_TRY_IGNORE(my_function(10));
  VX_TRY_IGNORE(my_function(-10));
  CHECK(VX_TRY_OR(my_function(17), -1000) == 4);
  CHECK(VX_TRY_OR(my_function(0), -1000) == 0);
  CHECK(VX_TRY_OR(my_function(-10), -1000) == -1000);

  {
    auto res = my_function(-10);
    CHECK(res.isError());
    res.consume();
  }

  // Check that Result works with move-only types.
  {
    Result<std::unique_ptr<int>> res = std::make_unique<int>(42);
    std::unique_ptr<int> ptr         = std::move(res).value();
    REQUIRE(ptr);
    CHECK(*ptr == 42);
  }
}
