#include "vx/Bitset.hpp"

#include "vx/PhysicalDevice.hpp"

#include <catch2/catch.hpp>

using namespace vx;

namespace vx {
  enum class TestBits : uint8_t {
    ABC = 0x01,
    DEF = 0x02,
    GHI = 0x04,
    JKL = 0x08,
  };

  enum class TestEnum : uint32_t {
    abc,
    def,
    ghi,
    jkl,
  };
}

VX_DETAIL_ENUM_FORMATTER_DECLARE(TestBits);
VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(TestBits) = {
    {TestBits::ABC, "ABC"},
    {TestBits::DEF, "DEF"},
    {TestBits::GHI, "GHI"},
    {TestBits::JKL, "JKL"},
};

VX_DETAIL_ENUM_FORMATTER_DECLARE(TestEnum);
VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(TestEnum) = {
    {TestEnum::abc, "abc"},
    {TestEnum::def, "def"},
    {TestEnum::ghi, "ghi"},
    {TestEnum::jkl, "jkl"},
};

TEST_CASE("enum helpers", "[Misc][enum]")
{
  CHECK(
      enumValues<TestBits>() ==
      std::vector{
          TestBits::ABC,
          TestBits::DEF,
          TestBits::GHI,
          TestBits::JKL,
      });
  CHECK(
      enumValues<TestEnum>() ==
      std::vector{
          TestEnum::abc,
          TestEnum::def,
          TestEnum::ghi,
          TestEnum::jkl,
      });

  CHECK(areFlagBits<TestBits>());
  CHECK(!areFlagBits<TestEnum>());

  {
    constexpr auto b1 = Bitset<TestBits>{};
    constexpr auto b2 = Bitset<TestBits>::create(TestBits::DEF, TestBits::GHI);
    constexpr auto b3 = Bitset<TestBits>::create(TestBits::DEF, TestBits::JKL);

    static_assert(b2 == createBitset(TestBits::DEF, TestBits::GHI));
    static_assert(b3 == createBitset(TestBits::DEF, TestBits::JKL));

    static_assert(!b1.test(TestBits::ABC));
    static_assert(!b1.test(TestBits::DEF));
    static_assert(!b1.test(TestBits::GHI));
    static_assert(!b1.test(TestBits::JKL));
    static_assert(b1.none());
    static_assert(!b1.any());

    static_assert(!b2.test(TestBits::ABC));
    static_assert(b2.test(TestBits::DEF));
    static_assert(b2.test(TestBits::GHI));
    static_assert(!b2.test(TestBits::JKL));
    static_assert(b2.any());
    static_assert(!b2.none());

    static_assert((b1 & b2) == b1);
    static_assert((b1 | b2) == b2);

    static_assert((b2 & b3) == Bitset<TestBits>::create(TestBits::DEF));
    {
      constexpr auto result =
          Bitset<TestBits>::create(TestBits::DEF, TestBits::GHI, TestBits::JKL);
      static_assert((b2 | b3) == result);
    }

    static_assert(b1.popcount() == 0);
    static_assert(b2.popcount() == 2);
    static_assert(b3.popcount() == 2);
    static_assert((b2 | b3).popcount() == 3);

    static_assert(
        Bitset<TestBits>::create(TestBits::ABC, TestBits::JKL) ==
        (Bitset<TestBits>{} | TestBits::ABC | TestBits::JKL));
  }
}
