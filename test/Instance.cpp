#include "vx/Instance.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;

TEST_CASE("Instance", "[Instance]")
{
  {
    auto const instVer = VX_TRY_REQUIRE(Instance::enumerateVersion());
    CHECK(instVer > Version::create(0));
  }

  {
    VX_TRY_REQUIRE(Instance::enumerateExtensionProperties());
  }

  {
    auto const layers = VX_TRY_REQUIRE(Instance::enumerateLayerProperties());
    for (auto const& layer: layers) {
      VX_TRY_REQUIRE(Instance::enumerateExtensionProperties(layer.layerName));
    }
  }

  {
    auto instResult = Instance::create();
    REQUIRE(!instResult.isError());
    CHECK(std::move(instResult).value().isValid());
  }

  {
    auto res = Instance::create(
        {},
        {
            .enabledLayerNames = {"nonExistentLayer"},
        });
    REQUIRE(res.isError());
    CHECK(std::move(res).error().code() == StatusCode::ERROR_LAYER_NOT_PRESENT);
  }
}
