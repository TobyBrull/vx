#include "vx/GlfwWindow.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

#include <chrono>
#include <thread>

using namespace vx;
using namespace std::literals::chrono_literals;

namespace {
  bool callbackRan = false;
}

TEST_CASE("GlfwWindow", "[Glfw][GlfwWindow]")
{
  auto const glfw = VX_TRY_REQUIRE(Glfw::init());

  auto const window = VX_TRY_REQUIRE(GlfwWindow::create({
      .title = "test: GlfwWindow",
  }));

  auto const fbSize = VX_TRY_REQUIRE(window.framebufferSize());
  CHECK(fbSize.width > 0);
  CHECK(fbSize.height > 0);

  GLFW::glfwSetWindowFocusCallback(
      window.handle, [](GLFWwindow*, int const) { callbackRan = true; });

  while (!callbackRan) {
    VX_TRY_REQUIRE(glfw.pollEvents());
  }
}
