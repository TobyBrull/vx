#include "vx/Enum.hpp"

#include <catch2/catch.hpp>

using namespace vx;

// MyEnum.h:
enum class MyEnum {
  Value1 = 1,
  Value2 = 2,
};

// MyEnum.ipp:
VX_DETAIL_ENUM_FORMATTER_DECLARE(MyEnum);

// MyEnum.cpp:
VX_DETAIL_ENUM_FORMATTER_DEFINE_MAP(MyEnum) = {
    {MyEnum::Value1, "Value1"},
    {MyEnum::Value2, "Value2"},
};

TEST_CASE("EnumFormatter", "[Enum][EnumFormatter]")
{
  CHECK(fmt::to_string(MyEnum::Value1) == "Value1");
  CHECK(fmt::to_string(MyEnum::Value2) == "Value2");
}
