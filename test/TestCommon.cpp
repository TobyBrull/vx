#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;
using namespace vx::test;

void
vx::test::throwFailureException()
{
  throw vx::test::FailureException();
}

Environment
vx::test::environment(Environment::Type const type)
{
  auto const [withGlfw, withSurf] = [type]() -> std::tuple<bool, bool> {
    switch (type) {
      case Environment::PURE_VULKAN:
        return {false, false};

      case Environment::WITH_GLFW:
        return {true, false};

      case Environment::WITH_SURFACE:
        return {true, true};
    }
    VX_UNREACHABLE();
  }();

  Environment retval;

  std::vector<std::string> extensions;
  if (withGlfw) {
    retval.glfw = VX_TRY_REQUIRE(Glfw::init());
    extensions  = VX_TRY_REQUIRE(Glfw::getRequiredInstanceExtensions());
  }

  retval.instance = VX_TRY_REQUIRE(Instance::create(
      {},
      {
          .enabledExtensionNames = extensions,
      }));

  if (withSurf) {
    retval.window = VX_TRY_REQUIRE(GlfwWindow::create({}));

    retval.surface =
        VX_TRY_REQUIRE(retval.window.createSurface(retval.instance));
  }

  retval.physDevInfo = VX_TRY_REQUIRE(
      PhysicalDeviceInfo::select(retval.instance, retval.surface));

  auto bitset = createBitset(QueueFlagBits::GRAPHICS, QueueFlagBits::TRANSFER);

  if (withSurf) {
    bitset = bitset | QueueFlagBits::SURFACE_SUPPORT;
  }

  retval.queueFamilyIndex = VX_TRY_REQUIRE(
      selectSingleQueue(retval.physDevInfo.queueFamilyProperties, bitset));

  retval.device = VX_TRY_REQUIRE(Device::createWithSingleQueue(
      retval.physDevInfo.physicalDevice, retval.queueFamilyIndex));

  retval.commandPool = VX_TRY_REQUIRE(CommandPool::create(
      retval.device,
      {
          .queueFamilyIndex = retval.queueFamilyIndex,
      }));

  retval.commandBuffer = VX_TRY_REQUIRE(CommandBuffer::allocate(
      retval.commandPool,
      {
          .commandBufferCount = 1,
      }));

  return retval;
}
