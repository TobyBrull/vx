#include "vx/Glfw.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

#include <chrono>
#include <thread>

using namespace vx;
using namespace std::literals::chrono_literals;

TEST_CASE("Glfw", "[Glfw]")
{
  {
    auto const glfw = VX_TRY_REQUIRE(Glfw::init());

    {
      auto err = Glfw::init();
      REQUIRE(err.isError());
      CHECK(
          std::move(err).error().code() == GlfwErrorCode::ALREADY_INITIALIZED);
    }
  }

  {
    auto const glfw = VX_TRY_REQUIRE(Glfw::init());
  }

  {
    auto const glfw = VX_TRY_REQUIRE(Glfw::init());

    VX_TRY_REQUIRE(Glfw::pollEvents());
    VX_TRY_REQUIRE(Glfw::waitEventsTimeout(0.001));

    std::jthread t([] {
      std::this_thread::sleep_for(1ms);
      VX_TRY_REQUIRE(Glfw::postEmptyEvent());
    });

    VX_TRY_REQUIRE(Glfw::waitEvents());

    auto const exts = VX_TRY_REQUIRE(Glfw::getRequiredInstanceExtensions());
    CHECK(!exts.empty());
    auto const findIt = std::ranges::find(exts, "VK_KHR_surface");
    CHECK(findIt != exts.end());

    {
      auto const inst = VX_TRY_REQUIRE(Instance::create(
          {},
          {
              .enabledExtensionNames = exts,
          }));

      auto const physDevInfo = VX_TRY_REQUIRE(PhysicalDeviceInfo::select(inst));

      for (auto const& qFamProps: physDevInfo.queueFamilyProperties) {
        VX_TRY_REQUIRE(Glfw::hasPresentationSupport(
            inst, physDevInfo.physicalDevice, qFamProps.index));
      }
    }
  }
}
