#include "vx/GlfwMonitor.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;

TEST_CASE("GlfwMonitor", "[Glfw][GlfwMonitor]")
{
  auto const glfw = VX_TRY_REQUIRE(Glfw::init());

  {
    auto const primMon = VX_TRY_REQUIRE(GlfwMonitor::getPrimary());
    REQUIRE(primMon.isValid());
  }

  {
    auto const mons = VX_TRY_REQUIRE(GlfwMonitor::enumerate());
    for (auto const& mon: mons) {
      REQUIRE(mon.isValid());
      auto const info = VX_TRY_REQUIRE(mon.getInfo());
      CHECK(info.physicalSizeMM.width > 0);
      CHECK(info.physicalSizeMM.height > 0);
      CHECK(info.workarea.extent.width > 0);
      CHECK(info.workarea.extent.height > 0);
      CHECK(info.contentScaleX > 0.0);
      CHECK(info.contentScaleY > 0.0);
      CHECK(!info.name.empty());
      auto const checkVidMode = [](GlfwVideoMode const& vidMode) {
        CHECK(vidMode.width > 0);
        CHECK(vidMode.height > 0);
        CHECK(vidMode.redBits > 0);
        CHECK(vidMode.greenBits > 0);
        CHECK(vidMode.blueBits > 0);
        CHECK(vidMode.refreshRate > 0);
      };
      CHECK(!info.availableVideoModes.empty());
      for (auto const& vidMode: info.availableVideoModes) {
        checkVidMode(vidMode);
      }
      CHECK(std::ranges::is_sorted(info.availableVideoModes));
      checkVidMode(info.currentVideoMode);
    }
  }
}
