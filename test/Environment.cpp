#include "vx/CommandBuffer.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;

TEST_CASE("test::Environment", "[Integration][Environment]")
{
  {
    auto env1 = test::environment(test::Environment::PURE_VULKAN);
    auto env2 = test::environment(test::Environment::PURE_VULKAN);
  }

  {
    auto env = test::environment(test::Environment::WITH_GLFW);
  }
}

TEST_CASE("test::Environment Glfw", "[Integration][Environment][Glfw]")
{
  {
    auto env = test::environment(test::Environment::WITH_SURFACE);
  }
}
