#include "vx/Failure.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

#include <csignal>

using namespace vx;

TEST_CASE("failure handler RAII", "[Failure]")
{
  static int count = 0;

  auto const testFailureHandlerCount = [] {
    count += 1;
    throw test::FailureException();
  };

  {
    FailureHandlerGuard const _{testFailureHandlerCount};
    CHECK_THROWS_AS(runFailureHandler(), test::FailureException);
    {
      FailureHandlerGuard const _{&test::throwFailureException};
      CHECK_THROWS_AS(runFailureHandler(), test::FailureException);
      CHECK_THROWS_AS(runFailureHandler(), test::FailureException);
      CHECK_THROWS_AS(runFailureHandler(), test::FailureException);
    }
    CHECK(count == 1);
  }
}

TEST_CASE("failure macros", "[macros][Failure]")
{
  FailureHandlerGuard const _{&test::throwFailureException};
  CHECK_THROWS_AS(runFailureHandler(), test::FailureException);

  // Unfortunately, the Catch2 CHECK_THROW... macros cannot be used to check
  // that a macro threw.
  {
    bool should_throw = false;
    bool caught       = false;
    try {
      SECTION("VX_REQUIRE(false)")
      {
        should_throw = true;
        VX_REQUIRE(false);
      }
      SECTION("VX_REQUIRE_FMT(false, fmt)")
      {
        should_throw = true;
        VX_REQUIRE_FMT(false, "message!");
      }
      SECTION("VX_REQUIRE_FMT(false, fmt, ...)")
      {
        should_throw = true;
        VX_REQUIRE_FMT(false, "value = {}!", 42);
      }
      SECTION("VX_REQUIRE(true)")
      {
        should_throw = false;
        VX_REQUIRE(true);
      }
      SECTION("VX_REQUIRE(true, fmt)")
      {
        should_throw = false;
        VX_REQUIRE_FMT(true, "message!");
      }
      SECTION("VX_REQUIRE_FMT(true, fmt, ...)")
      {
        should_throw = false;
        VX_REQUIRE_FMT(true, "value = {}!", 42);
      }
    }
    catch (test::FailureException const&) {
      caught = true;
    }
    CHECK(should_throw == caught);
  }
}

namespace {
  void on_sigabrt(int const)
  {
    std::exit(1);
  }
}
TEST_CASE("assertion macros 1", "[macros][Failure][.aborts]")
{
  signal(SIGABRT, &on_sigabrt);
  VX_ASSERT(false);
}
TEST_CASE("assertion macros 2", "[macros][Failure][.aborts]")
{
  signal(SIGABRT, &on_sigabrt);
  VX_ASSERT_FMT(false, "message!");
}
TEST_CASE("assertion macros 3", "[macros][Failure][.aborts]")
{
  signal(SIGABRT, &on_sigabrt);
  VX_ASSERT_FMT(false, "value = {}!", 42);
}
TEST_CASE("assertion macros 4", "[macros][Failure][.aborts]")
{
  signal(SIGABRT, &on_sigabrt);
  VX_VULKAN_SPEC(false);
}
TEST_CASE("assertion macros 5", "[macros][Failure][.aborts]")
{
  signal(SIGABRT, &on_sigabrt);
  VX_VULKAN_SPEC_FMT(false, "message!");
}
TEST_CASE("assertion macros 6", "[macros][Failure][.aborts]")
{
  signal(SIGABRT, &on_sigabrt);
  VX_VULKAN_SPEC_FMT(false, "value = {}!", 42);
}
TEST_CASE("assertion macros 7", "[macros][Failure][.aborts]")
{
  signal(SIGABRT, &on_sigabrt);
  VX_UNREACHABLE();
}

namespace {
  struct DtorStruct {
    static thread_local bool called;
    static void callback()
    {
      called = true;
    }
  };
  thread_local bool DtorStruct::called = false;
}

// libvx.so is always compiled without exceptions; some tests related to this
// are collected here.
TEST_CASE("failure handler with -fno-exceptions", "[Failure][exceptions]")
{
  FailureHandlerGuard const _{&test::throwFailureException};

  {
    DtorStruct::called = false;
    vx::detail::unitTestFunction(1, &DtorStruct::callback);
    CHECK(DtorStruct::called);
  }

  // Automatic variables inside vx will not be destructed automatically when
  // the failure handler is invoked. This means that with "-fno-exceptions"
  // and a throwing failure handler, the following code leaks memory.
  //
  // .. code::
  //
  //    int vx_function() {
  //      std::vector<double> vec = { /* ... */ };
  //      // ...
  //      VX_REQUIRE(vec.size() > 10);
  //      return vec.size();
  //    }
  //
  {
    DtorStruct::called = false;
    CHECK_THROWS_AS(
        vx::detail::unitTestFunction(-1, &DtorStruct::callback),
        test::FailureException);
    CHECK(DtorStruct::called == false);
  }
}
