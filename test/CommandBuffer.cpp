#include "vx/CommandBuffer.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;

TEST_CASE("CommandPool::CreateInfo::FlagBits", "[CommandBuffer][Bitset]")
{
  CHECK(areFlagBits<CommandPool::CreateInfo::FlagBits>());
}

TEST_CASE("CommandPool", "[CommandBuffer][CommandPool]")
{
  auto env = test::environment(test::Environment::PURE_VULKAN);

  CommandPool::CreateInfo const createInfo{
      .queueFamilyIndex = env.queueFamilyIndex,
  };

  auto cmdPool = VX_TRY_REQUIRE(CommandPool::create(env.device, createInfo));
  CHECK(cmdPool.createInfo == createInfo);
  auto cmdPoolCopy = std::move(cmdPool);
  CHECK(!cmdPool.isValid());
  CHECK(cmdPool.device == VK_NULL_HANDLE);
  CHECK(cmdPool.createInfo == CommandPool::CreateInfo{});
  cmdPool.destroy();
  CHECK(!cmdPool.isValid());
  CHECK(cmdPoolCopy.isValid());
  CHECK(cmdPoolCopy.device != VK_NULL_HANDLE);
  CHECK(cmdPoolCopy.createInfo == createInfo);
}

TEST_CASE("CommandBuffer", "[CommandBuffer][CommandPool]")
{
  auto env = test::environment(test::Environment::PURE_VULKAN);

  auto const result = VX_TRY_REQUIRE(CommandBuffer::allocate(
      env.commandPool,
      {
          .commandBufferCount = 1,
      }));
  CHECK(result.isValid());
}
