#include "vx/Vulkan.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;

TEST_CASE("common object types", "[Vulkan]")
{
  {
    auto const orig = UUID{
        .data = {
            0xff,
            0xee,
            0xdd,
            0xcc,
            0xbb,
            0xaa,
            0x99,
            0x88,
            0x77,
            0x66,
            0x55,
            0x44,
            0x03,
            0x22,
            0x01,
            0x00}};
    CHECK(orig == orig);
    CHECK(fmt::format("{:^34}", orig) == " ffeeddccbbaa99887766554403220100 ");
  }

  {
    auto const orig = Extent2D{.width = 2, .height = 3};
    auto const vlkn = detail::toVulkan(orig);
    CHECK(vlkn.width == 2);
    CHECK(vlkn.height == 3);
    CHECK(orig == detail::fromVulkan(vlkn));
    CHECK(fmt::format("{:4}", orig) == "{width=   2, height=   3}");
  }

  {
    auto const orig = Extent3D{.width = 2, .height = 3, .depth = 4};
    auto const vlkn = detail::toVulkan(orig);
    CHECK(vlkn.width == 2);
    CHECK(vlkn.height == 3);
    CHECK(vlkn.depth == 4);
    CHECK(orig == detail::fromVulkan(vlkn));
    CHECK(fmt::format("{:4}", orig) == "{width=   2, height=   3, depth=   4}");
  }

  {
    auto const orig = Offset2D{.x = -2, .y = 3};
    auto const vlkn = detail::toVulkan(orig);
    CHECK(vlkn.x == -2);
    CHECK(vlkn.y == 3);
    CHECK(orig == detail::fromVulkan(vlkn));
    CHECK(fmt::format("{:2}", orig) == "{x=-2, y= 3}");
  }

  {
    auto const orig = Offset3D{.x = -2, .y = 3, .z = 4};
    auto const vlkn = detail::toVulkan(orig);
    CHECK(vlkn.x == -2);
    CHECK(vlkn.y == 3);
    CHECK(vlkn.z == 4);
    CHECK(orig == detail::fromVulkan(vlkn));
    CHECK(fmt::format("{:2}", orig) == "{x=-2, y= 3, z= 4}");
  }

  {
    auto const offset = Offset2D{.x = -10, .y = 20};
    auto const extent = Extent2D{.width = 5, .height = 8};
    auto const orig   = Rect2D{.offset = offset, .extent = extent};
    auto const vlkn   = detail::toVulkan(orig);
    CHECK(vlkn.offset.x == -10);
    CHECK(vlkn.extent.height == 8);
    CHECK(orig == detail::fromVulkan(vlkn));
    CHECK(
        fmt::format("{:4}", orig) ==
        "{offset.x= -10, offset.y=  20, "
        "extent.width=   5, extent.height=   8}");
  }
}
