#include "vx/PhysicalDevice.hpp"

#include "TestCommon.hpp"

#include <catch2/catch.hpp>

using namespace vx;

TEST_CASE("QueueFlagBits", "[QueueFlagBits]")
{
  CHECK(areFlagBits<QueueFlagBits>());
}

TEST_CASE("PhysicalDevice::Type", "[PhysicalDevice][Type]")
{
  CHECK(fmt::to_string(PhysicalDevice::Type::OTHER) == "OTHER");
  CHECK(PhysicalDevice::Type::OTHER < PhysicalDevice::Type::CPU);
  std::hash<PhysicalDevice::Type>{}(PhysicalDevice::Type::CPU);

  CHECK(fmt::to_string(QueueFlagBits::COMPUTE) == "COMPUTE");
}

TEST_CASE("PhysicalDevice", "[PhysicalDevice]")
{
  auto const inst = VX_TRY_REQUIRE(Instance::create({}, {}));

  auto const physDevs = VX_TRY_REQUIRE(PhysicalDevice::enumerate(inst));
  CHECK(physDevs.size() >= 1);

  auto const layerProps = VX_TRY_REQUIRE(Instance::enumerateLayerProperties());

  for (auto const& pd: physDevs) {
    CHECK(pd.isValid());
    auto const props = pd.getProperties();
    CHECK(props.deviceName.size() > 0);
    CHECK(props.apiVersion > Version::create(0));

    auto const qFamProps = VX_TRY_REQUIRE(pd.getQueueFamilyProperties());
    CHECK(qFamProps.size() >= 1);
    for (size_t i = 0; i < qFamProps.size(); ++i) {
      CHECK(qFamProps[i].index == i);
    }

    {
      auto const exts = VX_TRY_REQUIRE(pd.enumerateExtensionProperties());
      for (auto const& ext: exts) {
        CHECK(!ext.extensionName.empty());
      }
    }

    for (auto const& layerProp: layerProps) {
      auto const exts =
          VX_TRY_REQUIRE(pd.enumerateExtensionProperties(layerProp.layerName));
      for (auto const& ext: exts) {
        CHECK(!ext.extensionName.empty());
      }
    }
  }
}

TEST_CASE("PhysicalDeviceInfo::isPreferable", "[PhysicalDevice]")
{
  PhysicalDeviceInfo lhs;
  PhysicalDeviceInfo rhs;

  lhs.properties.deviceType = PhysicalDevice::Type::INTEGRATED_GPU;
  lhs.properties.apiVersion = Version::create(2, 3, 4);
  lhs.index                 = 0;

  rhs.properties.deviceType = PhysicalDevice::Type::DISCRETE_GPU;
  rhs.properties.apiVersion = Version::create(3, 4, 5);
  rhs.index                 = 1;

  CHECK(!PhysicalDeviceInfo::isPreferable(lhs, lhs));
  CHECK(!PhysicalDeviceInfo::isPreferable(rhs, rhs));

  CHECK(!PhysicalDeviceInfo::isPreferable(lhs, rhs));
  CHECK(PhysicalDeviceInfo::isPreferable(rhs, lhs));

  rhs.properties.deviceType = lhs.properties.deviceType;

  CHECK(!PhysicalDeviceInfo::isPreferable(lhs, rhs));
  CHECK(PhysicalDeviceInfo::isPreferable(rhs, lhs));

  rhs.properties.apiVersion = lhs.properties.apiVersion;

  CHECK(PhysicalDeviceInfo::isPreferable(lhs, rhs));
  CHECK(!PhysicalDeviceInfo::isPreferable(rhs, lhs));

  lhs.index = 2;

  CHECK(!PhysicalDeviceInfo::isPreferable(lhs, rhs));
  CHECK(PhysicalDeviceInfo::isPreferable(rhs, lhs));
}

TEST_CASE("PhysicalDeviceInfo functions", "[PhysicalDevice]")
{
  auto const inst = VX_TRY_REQUIRE(Instance::create({}, {}));

  {
    auto const physDevInfos =
        VX_TRY_REQUIRE(PhysicalDeviceInfo::enumerate(inst));
    for (size_t i = 0; i < physDevInfos.size(); ++i) {
      CHECK(physDevInfos[i].index == i);
      CHECK(physDevInfos[i].physicalDevice.isValid());
      CHECK(physDevInfos[i].properties.deviceName.size() > 0);
      CHECK(physDevInfos[i].queueFamilyProperties.size() > 0);
    }
  }

  {
    auto const physDevInfo = VX_TRY_REQUIRE(PhysicalDeviceInfo::select(inst));
    CHECK(physDevInfo.physicalDevice.isValid());
  }
}

TEST_CASE("singleQueueCreateInfo", "[PhysicalDevice]")
{
  constexpr auto QFB_1 = QueueFlagBits::COMPUTE;
  constexpr auto QFB_2 = QueueFlagBits::GRAPHICS;
  constexpr auto QFB_3 = QueueFlagBits::PROTECTED;
  constexpr auto QFB_4 = QueueFlagBits::SPARSE_BINDING;
  constexpr auto QFB_5 = QueueFlagBits::TRANSFER;

  auto const queueFlags = createBitset(QFB_2, QFB_3);

  std::vector<std::string> const extensions = {"ExtensionA", "ExtensionB"};

  using QFP = PhysicalDevice::QueueFamilyProperties;

  {
    std::vector<QFP> qFamProps = {
        {
            .index      = 0,
            .queueFlags = createBitset(QFB_1, QFB_3, QFB_4, QFB_5),
            .queueCount = 3,
        },
        {
            .index      = 1,
            .queueFlags = createBitset(QFB_1, QFB_2, QFB_3),
            .queueCount = 1,
        },
        {
            .index      = 2,
            .queueFlags = createBitset(QFB_2, QFB_3),
            .queueCount = 14,
        },
    };

    auto const result =
        VX_TRY_REQUIRE(selectSingleQueue(qFamProps, queueFlags));

    CHECK(result == 1);
  }

  {
    std::vector<QFP> qFamProps = {
        {
            .index      = 0,
            .queueFlags = createBitset(QFB_2, QFB_3),
            .queueCount = 30,
        },
        {
            .index      = 1,
            .queueFlags = createBitset(QFB_1, QFB_2, QFB_3),
            .queueCount = 1,
        },
        {
            .index      = 2,
            .queueFlags = createBitset(QFB_2, QFB_3, QFB_4),
            .queueCount = 14,
        },
    };

    auto const result =
        VX_TRY_REQUIRE(selectSingleQueue(qFamProps, queueFlags));

    CHECK(result == 2);
  }

  {
    std::vector<QFP> qFamProps = {
        {
            .index      = 0,
            .queueFlags = createBitset(QFB_2, QFB_3),
            .queueCount = 30,
        },
        {
            .index      = 1,
            .queueFlags = createBitset(QFB_1, QFB_2, QFB_3),
            .queueCount = 14,
        },
        {
            .index      = 2,
            .queueFlags = createBitset(QFB_2, QFB_3, QFB_4),
            .queueCount = 14,
        },
    };

    auto const result =
        VX_TRY_REQUIRE(selectSingleQueue(qFamProps, queueFlags));

    CHECK(result == 1);
  }

  {
    std::vector<QFP> qFamProps = {
        {
            .index      = 0,
            .queueFlags = createBitset(QFB_2),
            .queueCount = 30,
        },
        {
            .index      = 1,
            .queueFlags = createBitset(QFB_1, QFB_3),
            .queueCount = 14,
        },
        {
            .index      = 2,
            .queueFlags = createBitset(QFB_3, QFB_4),
            .queueCount = 14,
        },
    };

    auto result = selectSingleQueue(qFamProps, queueFlags);
    REQUIRE(result.isError());
    CHECK(
        std::move(result).error().code() ==
        StatusCode::ERROR_NO_MATCHING_QUEUE_FAMILY);
  }
}
