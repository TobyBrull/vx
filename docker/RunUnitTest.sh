#/bin/bash

set -Eeuxo pipefail

script_abs_path=`realpath $0`
script_abs_dir=`dirname $script_abs_path`

if [[ $# -ne 3 ]]; then
  echo "Usage: $script_abs_path <source-dir> <build-dir> <toolchain-file>"
  exit 2
fi

source_dir=`realpath $1`
build_dir=`realpath $2`
toolchain_file=`realpath $3`

bash /StartHeadlessX11Daemon.sh

mkdir $build_dir

cmake \
  -S $source_dir \
  -B $build_dir \
  -DCMAKE_BUILD_TYPE=Debug \
  -DCMAKE_TOOLCHAIN_FILE=$toolchain_file \
  $script_abs_dir

make -C $build_dir -j`nproc` VERBOSE=1

cd $build_dir
export LSAN_OPTIONS=suppressions=$script_abs_dir/../cmake/toolchains/suppr.txt
ctest -C extended -E Glfw --verbose
