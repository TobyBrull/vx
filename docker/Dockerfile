# Install the development environment.
FROM archlinux as base
RUN pacman --noconfirm -Syu
RUN pacman --noconfirm -S fmt vulkan-swrast vulkan-icd-loader \
                          glfw-x11 xorg xorg-xinit xf86-video-dummy xterm
COPY docker/10-headless.conf /etc/X11/xorg.conf.d/
COPY docker/StartHeadlessX11Daemon.sh /
ENV DISPLAY=:0

FROM base as dev
RUN pacman --noconfirm -S base-devel cmake ccache git \
                          python python-pip python-setuptools
RUN pacman --noconfirm -S clang catch2 vulkan-devel vulkan-swrast
RUN pacman --noconfirm -S doxygen texlive-most graphviz
RUN pip install wheel \
 && pip install docutils rst2pdf Sphinx Breathe matplotlib

# Run the unit-tests (gcc)
FROM dev as unit_test_gcc
COPY . /app
RUN bash /app/docker/RunUnitTest.sh \
  /app /unit-test-gcc /app/cmake/toolchains/GccWithSanitizers.cmake

# Run the unit-tests (clang)
FROM dev as unit_test_clang
COPY . /app
RUN bash /app/docker/RunUnitTest.sh \
  /app /unit-test-clang /app/cmake/toolchains/ClangWithSanitizers.cmake

# Build the package.
FROM dev as build
COPY . /app
RUN bash /app/docker/RunBuild.sh /app /build /output

# Test the package.
FROM base as package_test
COPY --from=build /output /output
RUN cd / \
 && tar xf /output/*.tar.gz --strip-components=1 \
 && bash /StartHeadlessX11Daemon.sh \
 && /opt/vx/bin/vxInfo
