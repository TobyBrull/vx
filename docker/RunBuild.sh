#/bin/bash

set -Eeuxo pipefail

script_abs_path=`realpath $0`
script_abs_dir=`dirname $script_abs_path`

if [[ $# -ne 3 ]]; then
  echo "Usage: $script_abs_path <source-dir> <build-dir> <output-dir>"
  exit 2
fi

source_dir=`realpath $1`
build_dir=`realpath $2`
output_dir=`realpath $3`

bash /StartHeadlessX11Daemon.sh

mkdir $build_dir

cmake \
  -S $source_dir \
  -B $build_dir \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DCMAKE_INSTALL_PREFIX=/opt/vx \
  $script_abs_dir

make -C $build_dir -j`nproc` all doc VERBOSE=1

cd $build_dir
ctest -E Glfw --verbose
cpack

mkdir $output_dir
cp *.tar.gz $output_dir
