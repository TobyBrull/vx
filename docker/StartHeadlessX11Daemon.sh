#/bin/bash

set -Eeuxo pipefail

script_abs_path=`realpath $0`
script_abs_dir=`dirname $script_abs_path`

if [[ $# -ne 0 ]]; then
  echo "Usage: $script_abs_path"
fi

xinit &
sleep 1
