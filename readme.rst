vx - Modern C++ Vulkan Wrapper
==============================

Design Goals (in this order)
----------------------------

* First and foremost, vx should be a succinct and expressive reformulation of
  the Vulkan API in modern C++. The naming in vx should match the Vulkan API.
  Knowledge of vx should mostly be equivalent to knowledge of the Vulkan API.

* Allow seamless integration between vx and the Vulkan API, so that
  applications can implement parts in vx and other parts directly in the Vulkan
  API. It should be possible to switch each part individually between vx and
  the Vulkan API.

* The performance overhead should be relatively small.

* Add various convenience functions.

Dependencies
------------

* C++20 (with GCC's Statement Expression extension)
* CMake
* libfmt
* Vulkan
* GLFW
