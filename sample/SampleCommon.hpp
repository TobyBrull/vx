#ifndef __INCLUDE_GUARD_SAMPLE_SAMPLE_COMMON_HPP
#define __INCLUDE_GUARD_SAMPLE_SAMPLE_COMMON_HPP

#include <string.h>

#define VX_SAMPLE_MAIN_RESULT(appName)                                       \
  int main(int argc, char** argv)                                            \
  {                                                                          \
    bool testFlag = false;                                                   \
    for (int i = 0; i < argc; ++i) {                                         \
      if (strcmp(argv[i], "test") == 0) {                                    \
        testFlag = true;                                                     \
      }                                                                      \
    }                                                                        \
    auto result = mainResult(testFlag);                                      \
    if (result.isError()) {                                                  \
      fmt::print(stderr, appName " error: {}\n", std::move(result).error()); \
      return 2;                                                              \
    }                                                                        \
    else {                                                                   \
      result.consume();                                                      \
    }                                                                        \
    return 0;                                                                \
  }

#endif
