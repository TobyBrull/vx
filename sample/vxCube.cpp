#include "vx.hpp"

#include "SampleCommon.hpp"

#include <thread>

vx::ApplicationInfo const glfwInfo{
    .applicationName    = "Glfw",
    .applicationVersion = vx::Version::create(0, 0, 1),
};

vx::Result<void>
mainResult(bool const testFlag)
{
  auto const glfw = VX_TRY(vx::Glfw::init());

  auto const inst = VX_TRY(vx::Instance::create(
      glfwInfo,
      {
          .enabledExtensionNames =
              VX_TRY(vx::Glfw::getRequiredInstanceExtensions()),
      }));

  auto const window = VX_TRY(vx::GlfwWindow::create({
      .title = "vx::Glfw",
  }));

  auto const surf = VX_TRY(window.createSurface(inst));

  auto const physDevInfo = VX_TRY(vx::PhysicalDeviceInfo::select(inst, surf));

  auto const qIndex = VX_TRY(vx::selectSingleQueue(
      physDevInfo.queueFamilyProperties,
      createBitset(
          vx::QueueFlagBits::GRAPHICS,
          vx::QueueFlagBits::TRANSFER,
          vx::QueueFlagBits::SURFACE_SUPPORT)));

  fmt::print("Selected queue = {}\n", qIndex);
  fmt::print("Framebuffer size = {}\n", VX_TRY(window.framebufferSize()));

  int const repeat = testFlag ? 20 : 200;
  fmt::print("repeat = {}\n", repeat);
  for (int i = 0; i < repeat; ++i) {
    VX_TRY(glfw.waitEventsTimeout(0.01));
    if (VX_TRY(window.shouldClose())) {
      break;
    }
  }

  return {};
}

VX_SAMPLE_MAIN_RESULT("Glfw");
