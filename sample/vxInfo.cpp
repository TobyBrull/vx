#include "vx.hpp"

#include "SampleCommon.hpp"

vx::ApplicationInfo const vulkanInfoAppInfo{
    .applicationName    = "VulkanInfo",
    .applicationVersion = vx::Version::create(0, 0, 1),
};

vx::Result<void>
printLayer(vx::LayerProperties const& layer)
{
  fmt::print(
      " * {:40.40} (v{:8}): {}\n",
      layer.layerName,
      layer.specVersion,
      layer.description);

  auto const exts =
      VX_TRY(vx::Instance::enumerateExtensionProperties(layer.layerName));
  for (auto const& ext: exts) {
    fmt::print("   - {}\n", ext.extensionName);
  }

  return {};
}

void
printPhysicalDeviceInfo(vx::PhysicalDeviceInfo const& physDevInfo)
{
  fmt::print("{:=^60}\n", physDevInfo.index);
  fmt::print("Name:   {}\n", physDevInfo.properties.deviceName);
  fmt::print("Type:   {}\n", physDevInfo.properties.deviceType);
  fmt::print("API:    {}\n", physDevInfo.properties.apiVersion);
  fmt::print("Driver: {}\n", physDevInfo.properties.driverVersion);

  {
    fmt::print("\nQueue Families:\n");
    for (auto const& qFamProps: physDevInfo.queueFamilyProperties) {
      fmt::print("  [{}] {:2} queues: ", qFamProps.index, qFamProps.queueCount);
      for (auto const bit: vx::enumValues<vx::QueueFlagBits>()) {
        if (qFamProps.queueFlags.test(bit)) {
          fmt::print("{}, ", bit);
        }
        else {
          fmt::print("{: ^{}}", "", fmt::to_string(bit).size() + 2);
        }
      }
      fmt::print("\n");
    }
  }

  {
    constexpr int maxDeviceExtensions = 4;
    fmt::print("\nDevice Extensions\n");
    int count = 0;
    for (auto const& ext: physDevInfo.extensionProperties) {
      fmt::print(" * {}\n", ext.extensionName);
      if (count++ >= maxDeviceExtensions) {
        fmt::print(" * ...\n");
        break;
      }
    }
  }

  fmt::print("\n");
}

void
printGlfwVideoMode(vx::GlfwVideoMode const& vidMode, bool const current)
{
  fmt::print(
      " * {:5} x {:5}, ({:2},{:2},{:2}), {:3} Hz",
      vidMode.width,
      vidMode.height,
      vidMode.redBits,
      vidMode.greenBits,
      vidMode.blueBits,
      vidMode.refreshRate);
  if (current) {
    fmt::print(" <===");
  }
  fmt::print("\n");
}

void
printGlfwMonitorInfo(vx::GlfwMonitor::Info const& info, size_t const index)
{
  fmt::print("{:=^60}\n", (index == 0) ? "0[Primary]" : fmt::to_string(index));
  fmt::print("Name:               {}\n", info.name);
  fmt::print("Pos:                {}\n", info.pos);
  fmt::print("Workarea:           {}\n", info.workarea);
  fmt::print("Physical Size (mm): {}\n", info.physicalSizeMM);
  fmt::print(
      "Content Scale:      {{x={}, y={}}}\n",
      info.contentScaleX,
      info.contentScaleY);
  fmt::print("\nAvailable Video Modes:\n");
  for (auto const& vidMode: info.availableVideoModes) {
    printGlfwVideoMode(vidMode, vidMode == info.currentVideoMode);
  }
  fmt::print("\n");
}

vx::Result<void>
mainResult([[maybe_unused]] bool const testFlag)
{
  {
    auto const instVer = VX_TRY(vx::Instance::enumerateVersion());
    fmt::print("Instance version: {}\n", instVer);
  }

  {
    fmt::print("Instance extensions:\n");
    auto const exts = VX_TRY(vx::Instance::enumerateExtensionProperties());
    for (auto const& ext: exts) {
      fmt::print(" * {}\n", ext.extensionName);
    }
  }

  {
    fmt::print("\nLayers (with layer extensions):\n");
    auto const layers = VX_TRY(vx::Instance::enumerateLayerProperties());
    for (auto const& layer: layers) {
      VX_TRY(printLayer(layer));
    }
  }

  auto const inst = VX_TRY(vx::Instance::create(vulkanInfoAppInfo));

  {
    auto physDevInfos = VX_TRY(vx::PhysicalDeviceInfo::enumerate(inst));
    fmt::print("\nFound {} physical devices:\n", physDevInfos.size());
    for (auto const& physDevInfo: physDevInfos) {
      printPhysicalDeviceInfo(physDevInfo);
    }
  }

  {
    auto const glfw = VX_TRY(vx::Glfw::init());

    auto const monitors = VX_TRY(vx::GlfwMonitor::enumerate());
    fmt::print("\nFound {} monitors:\n", monitors.size());
    for (size_t i = 0; i < monitors.size(); ++i) {
      auto const info = VX_TRY(monitors[i].getInfo());
      printGlfwMonitorInfo(info, i);
    }
  }

  return {};
}

VX_SAMPLE_MAIN_RESULT("VulkanInfo");
