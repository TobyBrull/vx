find_program(
  CLANG_TIDY_EXECUTABLE
  NAMES clang-tidy
  )

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  ClangTidy
  "Failed to find clang-tidy executable"
  CLANG_TIDY_EXECUTABLE
  )

macro(add_clang_tidy)
  cmake_parse_arguments(PARAM "" "TARGET_PREFIX" "FILES" ${ARGN})

  add_test(
    NAME "${PARAM_TARGET_PREFIX}-clang-tidy"
    COMMAND "${CLANG_TIDY_EXECUTABLE}"
        -p "${CMAKE_BINARY_DIR}/compile_commands.json" ${PARAM_FILES}
    CONFIGURATIONS "extended"
    WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
    )
endmacro()

